exec VYMAZ_DATA_VSECH_TABULEK;

--8 zaznamu EDICE

insert into EDICE (ID_EDICE, NAZEV, VYDANI)
values (1, 'Summer-2018', to_date('01-APR-18', 'DD-MON-RR'));
insert into EDICE (ID_EDICE, NAZEV, VYDANI)
values (3, 'Spring-2018', to_date('01-JAN-18', 'DD-MON-RR'));
insert into EDICE (ID_EDICE, NAZEV, VYDANI)
values (2, 'Summer-2019', to_date('02-APR-19', 'DD-MON-RR'));
insert into EDICE (ID_EDICE, NAZEV, VYDANI)
values (4, 'Spring-2019', to_date('09-JAN-19', 'DD-MON-RR'));
insert into EDICE (ID_EDICE, NAZEV, VYDANI)
values (5, 'Autumn-2019', to_date('19-JUL-19', 'DD-MON-RR'));
insert into EDICE (ID_EDICE, NAZEV, VYDANI)
values (6, 'Autumn-2018', to_date('08-JUL-18', 'DD-MON-RR'));
insert into EDICE (ID_EDICE, NAZEV, VYDANI)
values (7, 'Winter-2019', to_date('07-NOV-19', 'DD-MON-RR'));
insert into EDICE (ID_EDICE, NAZEV, VYDANI)
values (8, 'Winter-2018', to_date('15-NOV-18', 'DD-MON-RR'));
commit;


--6 zaznamu KATEGORIE

insert into KATEGORIE (ID_KATEGORIE, NAZEV)
values (1, 'jacket');
insert into KATEGORIE (ID_KATEGORIE, NAZEV)
values (2, 'socks');
insert into KATEGORIE (ID_KATEGORIE, NAZEV)
values (3, 'hats');
insert into KATEGORIE (ID_KATEGORIE, NAZEV)
values (4, 'neoprene');
insert into KATEGORIE (ID_KATEGORIE, NAZEV)
values (5, 'shorts');
insert into KATEGORIE (ID_KATEGORIE, NAZEV)
values (6, 'pants');

commit;


--70 zaznamu PRODUKT

insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (1, 'MembraneBlue',
        'lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo',
        8,
        'non velit donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere',
        4);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (2, 'Cimetidine',
        'tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante',
        3,
        'maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in',
        3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (3, 'Lovenox', null, 8,
        'fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris',
        3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (4, 'IFEREX 150',
        'gravida nisi at nibh in hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer',
        5, null, 3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (5, 'REVITALIZING C I2PL MASK',
        'in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus',
        3,
        'venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl',
        3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (6, 'Amoeba Tox', null, 6, null, 1);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (7, 'Vitalizer',
        'consectetuer adipiscing elit proin risus praesent lectus vestibulum quam sapien varius ut blandit non', 5,
        null, 2);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (8, 'Synthroid', 'nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium', 6,
        'duis ac nibh fusce lacus purus aliquet at feugiat non pretium quis lectus suspendisse potenti in', 6);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (9, 'Absent Minded',
        'vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia', 5,
        'tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in est risus auctor sed',
        2);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (10, 'Nortriptyline Hydrochloride',
        'consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum',
        7, null, 6);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (11, 'Hackberry Pollen',
        'consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus',
        4, null, 5);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (12, 'Sprayology PMS Support',
        'et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien',
        8,
        'mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel accumsan',
        4);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (13, 'Corn Grain',
        'ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis', 2,
        'volutpat sapien arcu sed augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut',
        5);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (14, 'Nitrogen',
        'sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend', 6,
        'felis sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu',
        3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (15, 'Icy Hot Medicated No Mess Applicator',
        'ornare consequat lectus in est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla nisl nunc nisl',
        1,
        'sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum',
        4);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (16, 'Shopko Antibacterial Foaming Hand Cleanser',
        'eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien', 5,
        'erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod',
        2);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (17, 'Hourglass Illusion Tinted Moisturizer Warm Beige',
        'hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt',
        2, 'id ligula suspendisse ornare consequat lectus in est risus auctor sed tristique in tempus sit amet sem',
        3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (18, 'Neutrogena Mineral Sheers',
        'odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus', 6,
        'ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam',
        2);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (19, 'HOUSEFLY FOR DIAGNOSTIC USE ONLY',
        'montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa',
        5,
        'donec ut mauris eget massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in',
        3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (20, 'Sambucol Cold and Flu Night Time Syrup',
        'justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis', 1, null, 5);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (21, 'Dove', null, 5, null, 5);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (22, 'Ephedrine Sulfate',
        'penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis',
        3, null, 2);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (23, 'Childrens Allergy',
        'in purus eu magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum',
        8,
        'convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel',
        5);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (24, 'Molds, Rusts and Smuts, Epicoccum nigrum',
        'magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu', 6, null, 5);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (25, 'Care One Pain Relief',
        'arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci',
        2, null, 5);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (26, 'Levothyroxine Sodium',
        'sit amet consectetuer adipiscing elit proin risus praesent lectus vestibulum quam sapien varius ut blandit',
        4,
        'magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar',
        5);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (27, 'Caffeine Citrate',
        'posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus', 5,
        'aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero',
        6);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (28, 'Dexrazoxane Hydrochloride',
        'imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum',
        4,
        'luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin',
        3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (29, 'Gastrolean',
        'odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce',
        2, null, 6);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (30, 'Anhydrous SPF 30',
        'ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis', 2,
        'consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit',
        5);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (31, 'Cleocin Phosphate',
        'consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere',
        6, null, 4);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (32, 'Antiseptic Mouth Rinse',
        'rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel',
        1, null, 3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (33, 'Hymenoptera Venom Diagnostic',
        'egestas metus aenean fermentum donec ut mauris eget massa tempor convallis nulla neque libero', 3, null,
        6);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (34, 'Nisoldipine',
        'donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit', 3,
        'mauris lacinia sapien quis libero nullam sit amet turpis elementum ligula vehicula consequat morbi a ipsum integer a',
        4);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (35, 'Diltiazem Hydrochloride',
        'lacinia sapien quis libero nullam sit amet turpis elementum ligula vehicula consequat morbi a', 1, null,
        2);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (36, 'LBEL HYDRATESS',
        'sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur', 6,
        'odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in',
        2);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (37, 'Acetylcysteine',
        'vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio',
        4, 'non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla', 2);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (38, 'mycophenolate mofetil', null, 2,
        'vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum',
        4);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (39, 'Post-Dental Visit',
        'dui luctus rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus purus aliquet at', 4,
        'lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis',
        3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (40, 'Korlym', 'donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac', 1,
        'dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis',
        3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (41, 'Treatment Set TS332091',
        'mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea',
        2,
        'metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce',
        1);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (42, 'Oxacillin',
        'donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus',
        6,
        'ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
        3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (43, 'Glycopyrrolate',
        'dolor sit amet consectetuer adipiscing elit proin risus praesent lectus vestibulum quam sapien varius ut',
        5, 'dolor vel est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac', 3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (44, 'SOLAR SENSE CLEAR ZINC SUNSCREEN FACE and BODY BROAD SPECTRUM SPF 70',
        'at dolor quis odio consequat varius integer ac leo pellentesque ultrices', 6,
        'nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras',
        4);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (45, 'Desert Beige',
        'cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel', 7, null, 5);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (46, 'DIORSKIN NUDE BB CREME NUDE GLOW SKIN-PERFECTING BEAUTY BALM SUNSCREEN BROAD SPECTRUM SPF10 Tint 4',
        'sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in',
        1, null, 3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (47, 'Stemphylium', null, 3, null, 2);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (48, 'Clonazepam',
        'pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat',
        3, null, 6);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (49, 'Good Sense Sore Throat',
        'turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis aliquam',
        7,
        'in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin',
        1);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (50, 'SKIN CAVIAR CONCEALER FOUNDATION SUNSCREEN SPF 15 - Creme Blush',
        'ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque', 1,
        'dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia',
        2);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (51, 'Levetiracetam',
        'magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere', 3,
        'quis libero nullam sit amet turpis elementum ligula vehicula consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus',
        3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (52, 'Docusate Sodium',
        'ac nibh fusce lacus purus aliquet at feugiat non pretium quis lectus suspendisse potenti in eleifend quam a',
        8, null, 1);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (53, 'Hamamelis Aesculus Varicose Relief',
        'aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt', 4,
        'sit amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum ac tellus semper interdum',
        5);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (54, 'Pain Reliever PM', 'ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio justo', 4,
        null, 1);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (55, 'ADRENOPLEX', 'nunc purus phasellus in felis donec semper sapien a libero nam dui proin', 5,
        'consectetuer adipiscing elit proin risus praesent lectus vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum',
        1);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (56, 'equate pain and fever',
        'penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum', 3, null, 6);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (57, 'berkley and jensen ibuprofen',
        'sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent', 7,
        'lacus curabitur at ipsum ac tellus semper interdum mauris ullamcorper purus sit amet nulla quisque', 2);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (58, 'MIDAZOLAM Hydrochloride',
        'erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam', 6,
        'dapibus augue vel accumsan tellus nisi eu orci mauris lacinia sapien quis libero nullam sit amet turpis elementum ligula vehicula consequat morbi a',
        3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (59, 'Face and Body Sunscreen Spray',
        'ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc', 4,
        'ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit',
        4);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (60, 'Aveeno Active Naturals Positively Ageless Correcting Tinted Moisturizer',
        'curabitur in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt', 2,
        'id justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio justo sollicitudin',
        4);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (61, 'Circulopath', 'at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non', 8,
        'vehicula consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie',
        1);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (62, 'Trichophyton for Intradermal Skin Testing',
        'vitae consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc',
        6,
        'lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec',
        4);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (63, 'ACYCLOVIR',
        'pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem', 7, null, 4);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (64, 'LOXITANE',
        'malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet', 6,
        null, 3);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (65, 'Acetaminophen and Codeine Phosphate',
        'parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent',
        5,
        'in purus eu magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum',
        4);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (66, 'Amitriptyline Hydrochloride',
        'eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl', 7,
        'libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim', 1);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (67, 'good neighbor pharmacy nasal',
        'dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices', 3,
        'phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id', 6);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (68, 'Diclofenac Sodium',
        'dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien',
        4,
        'eleifend quam a odio in hac habitasse platea dictumst maecenas ut massa quis augue luctus tincidunt nulla mollis molestie lorem quisque',
        2);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (69, 'DEMEROL', 'ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse', 1,
        'viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non',
        4);
insert into PRODUKT (ID_PRODUKT, NAZEV, POPIS, ID_EDICE, POZNAMKA, ID_KATEGORIE)
values (70, 'Glutose', 'sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque duis bibendum', 6,
        'rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet',
        6);
commit;


--19 zaznamu souvislost

insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (7, 47);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (21, 45);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (10, 33);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (24, 38);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (6, 38);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (14, 45);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (22, 38);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (19, 32);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (17, 36);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (5, 30);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (25, 38);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (4, 26);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (10, 28);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (20, 35);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (3, 49);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (4, 40);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (20, 28);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (11, 38);
insert into SOUVISLOST (ID_PRODUKT, ID_PRODUKT1)
values (10, 43);
commit;


--17 zaznamu barva


insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (1, 'Violet', 979);
insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (2, 'Purple', 687);
insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (3, 'Maroon', 866);
insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (4, 'Red', 976);
insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (5, 'Puce', 306);
insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (6, 'Green', 846);
insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (7, 'Khaki', 142);
insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (8, 'Maroon', 635);
insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (9, 'Aquamarine', 959);
insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (10, 'Blue', 972);
insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (11, 'Mauv', 753);
insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (12, 'Teal', 438);
insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (13, 'Mauv', 986);
insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (14, 'Crimson', 485);
insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (15, 'Red', 518);
insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (16, 'Goldenrod', 924);
insert into BARVA (ID_BARVA, NAZEV, BARVA)
values (17, 'Indigo', 652);

commit;


--3 zaznamu pohlavi

insert into POHLAVI (ID_POHLAVI, NAZEV)
values (1, 'Male');
insert into POHLAVI (ID_POHLAVI, NAZEV)
values (2, 'Female');
insert into POHLAVI (ID_POHLAVI, NAZEV)
values (3, 'Other');

commit;


--6 zaznamu velikost


insert into VELIKOST (ID_VELIKOST, ZNACKA, INFORMACE)
values (1, 'XS',
        'Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.');
insert into VELIKOST (ID_VELIKOST, ZNACKA, INFORMACE)
values (2, 'S', null);
insert into VELIKOST (ID_VELIKOST, ZNACKA, INFORMACE)
values (3, 'M', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna.');
insert into VELIKOST (ID_VELIKOST, ZNACKA, INFORMACE)
values (4, 'L',
        'Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.');
insert into VELIKOST (ID_VELIKOST, ZNACKA, INFORMACE)
values (5, 'XL',
        'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique.');
insert into VELIKOST (ID_VELIKOST, ZNACKA, INFORMACE)
values (6, 'XXL',
        'Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.');

commit;


--300 zaznamu specifikace

insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (1, 34, null, 900, 1, 23, 2, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (2, 70, null, 370, 1, 40, 1, 15);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (3, 8, null, 77, 2, 4, 3, 8);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (4, 37, null, 799, 2, 61, 3, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (5, 38, null, 946, 4, 11, 3, 8);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (6, 67, 'morbi ut odio cras mi pede malesuada in imperdiet et', 918, 6, 70, 3, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (7, 35, null, 754, 5, 65, 1, 6);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (8, 13, null, 726, 6, 39, 1, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (9, 7, null, 561, 4, 48, 3, 6);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (10, 33, null, 389, 3, 3, 1, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (11, 8, null, 600, 3, 69, 1, 12);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (12, 4, null, 251, 5, 8, 3, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (13, 25, null, 65, 4, 28, 3, 12);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (14, 8, 'erat id mauris vulputate elementum nullam varius nulla facilisi', 537, 5, 46, 2, 7);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (15, 41, null, 313, 6, 31, 2, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (16, 59, null, 74, 1, 49, 1, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (17, 88, null, 603, 2, 41, 3, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (18, 8, null, 74, 5, 66, 3, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (19, 63, null, 837, 5, 37, 1, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (20, 40, null, 696, 5, 32, 2, 13);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (21, 56, null, 617, 3, 43, 2, 13);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (22, 13, 'duis consequat dui nec nisi volutpat eleifend donec', 525, 5, 41, 1, 3);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (23, 54, 'amet lobortis sapien sapien non mi integer ac neque', 427, 1, 53, 2, 8);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (24, 71, null, 743, 3, 61, 1, 13);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (25, 14, 'eros vestibulum ac est lacinia', 247, 3, 69, 3, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (26, 2, null, 265, 4, 23, 2, 9);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (27, 45, null, 522, 6, 14, 1, 15);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (28, 37, null, 631, 4, 26, 1, 13);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (29, 74, null, 43, 6, 32, 3, 8);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (30, 0, null, 511, 1, 40, 2, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (31, 32, 'justo in hac habitasse platea dictumst', 572, 6, 52, 1, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (32, 52, null, 293, 3, 29, 3, 14);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (33, 45, null, 431, 6, 44, 2, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (34, 20, 'porttitor pede justo eu massa donec dapibus', 993, 6, 33, 2, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (35, 27, null, 908, 3, 59, 2, 3);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (36, 85, null, 453, 2, 35, 1, 8);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (37, 43, null, 65, 4, 69, 3, 15);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (38, 71, null, 206, 2, 11, 3, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (39, 17, 'lacinia sapien quis libero nullam sit amet turpis', 625, 3, 42, 1, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (40, 29, 'ut volutpat sapien arcu sed augue', 454, 4, 9, 2, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (41, 28, null, 948, 2, 30, 1, 15);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (42, 74, null, 214, 4, 68, 3, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (43, 31, null, 377, 3, 64, 2, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (44, 12, 'turpis a pede posuere nonummy', 807, 2, 21, 3, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (45, 28, null, 214, 6, 32, 1, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (46, 81, null, 835, 1, 32, 1, 9);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (47, 89, 'hac habitasse platea dictumst maecenas ut massa quis augue', 651, 5, 4, 2, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (48, 52, null, 541, 1, 58, 1, 9);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (49, 1, null, 32, 6, 30, 1, 15);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (50, 37, null, 42, 1, 64, 1, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (51, 28, null, 570, 6, 31, 1, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (52, 38, null, 971, 3, 51, 2, 15);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (53, 71, null, 865, 4, 6, 2, 13);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (54, 61, null, 574, 4, 50, 2, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (55, 99, null, 281, 5, 68, 3, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (56, 89, null, 428, 5, 66, 3, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (57, 35, null, 88, 1, 49, 2, 6);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (58, 89, null, 873, 4, 66, 3, 14);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (59, 98, null, 941, 1, 27, 2, 14);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (60, 51, null, 856, 4, 13, 1, 8);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (61, 4, null, 802, 1, 69, 3, 8);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (62, 49, null, 651, 1, 70, 2, 13);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (63, 20, null, 87, 2, 69, 3, 12);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (64, 66, null, 754, 2, 61, 2, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (65, 14, null, 900, 3, 44, 1, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (66, 24, null, 785, 4, 13, 2, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (67, 1, null, 656, 1, 56, 1, 6);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (68, 54, null, 653, 4, 61, 1, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (69, 33, null, 464, 6, 47, 1, 3);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (70, 47, null, 92, 3, 33, 1, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (71, 73, null, 923, 1, 48, 1, 7);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (72, 43, null, 212, 4, 52, 1, 7);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (73, 22, null, 717, 3, 49, 2, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (74, 96, null, 805, 2, 28, 2, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (75, 82, null, 353, 5, 8, 2, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (76, 66, null, 622, 2, 28, 3, 9);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (77, 56, null, 209, 2, 4, 2, 3);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (78, 56, null, 91, 4, 25, 1, 6);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (79, 84, 'vestibulum quam sapien varius ut blandit non', 429, 2, 15, 2, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (80, 56, null, 694, 3, 16, 2, 13);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (81, 20, null, 450, 6, 58, 2, 15);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (82, 55, null, 318, 2, 43, 1, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (83, 34, 'eget massa tempor convallis nulla', 500, 5, 5, 3, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (84, 88, null, 745, 6, 34, 1, 13);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (85, 14, null, 292, 4, 1, 1, 9);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (86, 24, null, 623, 2, 68, 2, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (87, 16, null, 634, 2, 58, 1, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (88, 37, 'mi pede malesuada in imperdiet et commodo vulputate justo in', 323, 1, 69, 1, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (89, 75, null, 926, 3, 68, 3, 8);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (90, 83, null, 275, 5, 53, 2, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (91, 78, null, 27, 1, 62, 2, 9);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (92, 59, null, 659, 5, 39, 2, 7);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (93, 67, null, 877, 4, 64, 3, 3);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (94, 74, 'tincidunt ante vel ipsum praesent blandit lacinia', 593, 2, 51, 1, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (95, 80, 'sapien arcu sed augue aliquam', 302, 2, 56, 1, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (96, 49, null, 988, 2, 66, 2, 7);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (97, 65, null, 646, 6, 45, 1, 15);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (98, 65, 'mauris enim leo rhoncus sed vestibulum sit', 431, 6, 25, 3, 14);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (99, 53, null, 233, 1, 50, 3, 12);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (100, 32, null, 443, 2, 50, 2, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (101, 93, null, 878, 2, 37, 1, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (102, 90, null, 897, 5, 11, 1, 15);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (103, 52, null, 235, 2, 25, 3, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (104, 100, 'quis libero nullam sit amet turpis elementum', 492, 2, 58, 3, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (105, 15, 'erat id mauris vulputate elementum nullam varius nulla facilisi', 887, 1, 44, 3, 14);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (106, 25, 'at velit vivamus vel nulla eget', 724, 6, 64, 3, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (107, 4, null, 745, 6, 2, 1, 8);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (108, 16, null, 259, 1, 12, 3, 13);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (109, 7, null, 61, 5, 39, 1, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (110, 65, null, 835, 4, 13, 3, 15);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (111, 75, null, 40, 3, 1, 3, 6);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (112, 83, 'hendrerit at vulputate vitae nisl aenean lectus', 343, 4, 21, 3, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (113, 73, null, 745, 5, 16, 1, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (114, 77, 'et tempus semper est quam pharetra magna ac', 741, 1, 19, 1, 12);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (115, 71, null, 534, 6, 26, 3, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (116, 12, null, 67, 3, 46, 2, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (117, 98, null, 995, 1, 36, 3, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (118, 97, null, 400, 2, 10, 2, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (119, 15, null, 226, 1, 38, 1, 6);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (120, 82, 'luctus et ultrices posuere cubilia', 523, 6, 3, 1, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (121, 76, null, 705, 3, 70, 1, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (122, 52, null, 856, 5, 7, 3, 13);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (123, 61, null, 283, 4, 45, 2, 7);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (124, 22, 'morbi non quam nec dui', 825, 6, 18, 1, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (125, 44, null, 264, 1, 42, 1, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (126, 64, null, 290, 1, 35, 3, 3);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (127, 68, 'non lectus aliquam sit amet diam in magna bibendum', 430, 4, 17, 1, 15);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (128, 87, null, 548, 6, 44, 1, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (129, 30, null, 405, 1, 40, 1, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (130, 68, null, 371, 6, 60, 1, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (131, 19, 'morbi a ipsum integer a nibh in quis', 809, 2, 29, 3, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (132, 36, null, 451, 4, 19, 3, 7);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (133, 14, null, 414, 3, 48, 2, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (134, 7, null, 993, 2, 41, 3, 13);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (135, 22, null, 568, 2, 45, 2, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (136, 51, null, 765, 3, 42, 1, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (137, 11, null, 705, 4, 70, 3, 3);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (138, 45, 'vestibulum ante ipsum primis in faucibus orci luctus et ultrices', 552, 6, 68, 3, 7);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (139, 32, null, 676, 3, 36, 3, 13);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (140, 96, 'consequat morbi a ipsum integer', 597, 4, 46, 1, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (141, 14, null, 697, 2, 25, 1, 7);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (142, 99, null, 519, 1, 21, 2, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (143, 63, null, 243, 4, 44, 2, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (144, 67, null, 732, 3, 61, 1, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (145, 1, 'id pretium iaculis diam erat fermentum justo', 918, 3, 9, 1, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (146, 89, null, 433, 3, 57, 1, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (147, 6, null, 431, 4, 32, 2, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (148, 92, null, 290, 5, 11, 3, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (149, 40, 'platea dictumst etiam faucibus cursus urna ut', 293, 6, 36, 2, 14);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (150, 31, null, 765, 5, 30, 2, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (151, 96, null, 773, 2, 47, 2, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (152, 1, null, 966, 6, 35, 3, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (153, 44, null, 531, 4, 4, 1, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (154, 8, null, 506, 2, 70, 3, 14);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (155, 81, 'imperdiet et commodo vulputate justo in blandit', 322, 2, 14, 3, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (156, 33, null, 298, 6, 70, 2, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (157, 64, null, 633, 6, 40, 3, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (158, 59, null, 979, 3, 58, 1, 8);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (159, 46, null, 628, 3, 62, 3, 6);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (160, 17, null, 473, 2, 70, 1, 13);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (161, 52, null, 855, 3, 57, 3, 14);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (162, 96, null, 890, 3, 39, 2, 15);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (163, 60, null, 426, 6, 10, 1, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (164, 70, null, 731, 6, 4, 1, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (165, 49, null, 947, 6, 46, 3, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (166, 17, null, 680, 5, 11, 1, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (167, 31, null, 488, 4, 52, 3, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (168, 11, null, 755, 3, 21, 1, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (169, 17, null, 576, 5, 6, 3, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (170, 37, null, 97, 3, 19, 2, 8);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (171, 31, null, 272, 3, 30, 1, 9);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (172, 50, null, 460, 4, 62, 3, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (173, 90, null, 800, 5, 53, 2, 6);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (174, 99, 'curabitur at ipsum ac tellus', 717, 3, 66, 2, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (175, 30, null, 681, 4, 58, 2, 3);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (176, 11, null, 665, 2, 11, 2, 3);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (177, 21, null, 882, 5, 49, 2, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (178, 58, 'porttitor lorem id ligula suspendisse ornare', 349, 5, 16, 1, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (179, 12, null, 238, 1, 24, 3, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (180, 96, null, 771, 2, 2, 2, 14);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (181, 10, 'lacinia eget tincidunt eget tempus vel pede morbi', 336, 1, 48, 1, 15);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (182, 59, null, 997, 6, 26, 3, 7);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (183, 77, null, 211, 2, 39, 1, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (184, 42, null, 982, 2, 30, 1, 9);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (185, 69, null, 452, 1, 63, 2, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (186, 34, null, 504, 5, 20, 2, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (187, 39, 'erat eros viverra eget congue eget semper rutrum nulla nunc', 253, 6, 61, 2, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (188, 70, 'ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris', 540, 2, 7, 2, 7);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (189, 1, null, 250, 5, 45, 3, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (190, 39, null, 206, 5, 66, 2, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (191, 19, 'nonummy maecenas tincidunt lacus at velit', 53, 2, 4, 3, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (192, 51, null, 790, 4, 52, 3, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (193, 33, null, 321, 5, 22, 3, 8);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (194, 85, null, 21, 3, 5, 1, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (195, 86, null, 408, 3, 69, 2, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (196, 47, null, 502, 5, 17, 1, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (197, 83, null, 605, 3, 23, 2, 9);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (198, 24, null, 534, 4, 49, 1, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (199, 56, null, 476, 3, 43, 1, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (200, 58, null, 209, 6, 37, 2, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (201, 49, null, 49, 4, 7, 2, 3);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (202, 11, 'primis in faucibus orci luctus et ultrices posuere cubilia', 94, 1, 3, 2, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (203, 90, null, 263, 6, 24, 3, 14);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (204, 60, null, 969, 6, 36, 2, 12);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (205, 76, 'quis tortor id nulla ultrices aliquet maecenas', 790, 2, 6, 2, 8);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (206, 15, null, 519, 2, 13, 2, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (207, 21, null, 322, 4, 9, 1, 15);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (208, 35, null, 759, 3, 68, 3, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (209, 15, null, 217, 1, 3, 2, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (210, 71, null, 382, 4, 7, 2, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (211, 60, null, 971, 2, 2, 1, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (212, 62, null, 807, 3, 64, 1, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (213, 28, null, 638, 5, 58, 3, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (214, 54, null, 871, 1, 51, 2, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (215, 67, null, 28, 6, 5, 3, 6);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (216, 78, null, 663, 5, 25, 1, 13);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (217, 75, 'nunc purus phasellus in felis donec semper', 560, 1, 65, 1, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (218, 45, 'consequat nulla nisl nunc nisl duis bibendum felis', 741, 1, 13, 2, 14);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (219, 32, null, 415, 3, 68, 1, 7);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (220, 97, null, 407, 4, 70, 1, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (221, 56, null, 960, 5, 54, 2, 9);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (222, 42, null, 676, 6, 46, 2, 9);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (223, 46, null, 258, 2, 2, 3, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (224, 26, null, 206, 6, 15, 2, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (225, 89, null, 50, 5, 64, 1, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (226, 0, null, 329, 4, 9, 2, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (227, 45, 'orci nullam molestie nibh in lectus', 851, 1, 52, 2, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (228, 19, null, 598, 1, 30, 1, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (229, 38, null, 585, 3, 12, 1, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (230, 30, null, 234, 5, 63, 3, 3);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (231, 11, null, 293, 1, 26, 3, 3);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (232, 35, null, 326, 1, 61, 2, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (233, 39, null, 398, 6, 67, 2, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (234, 79, null, 443, 1, 8, 3, 14);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (235, 62, null, 561, 1, 65, 3, 9);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (236, 62, null, 75, 3, 44, 2, 3);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (237, 93, 'arcu libero rutrum ac lobortis vel', 239, 3, 24, 1, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (238, 78, 'nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel', 796, 2, 21, 2, 6);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (239, 46, null, 814, 5, 40, 3, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (240, 67, null, 303, 4, 39, 2, 14);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (241, 14, null, 843, 6, 26, 3, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (242, 30, 'justo sit amet sapien dignissim vestibulum vestibulum', 721, 5, 67, 2, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (243, 74, null, 745, 1, 45, 1, 14);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (244, 42, null, 88, 6, 61, 2, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (245, 29, null, 734, 1, 24, 1, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (246, 48, null, 310, 6, 54, 3, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (247, 87, 'libero rutrum ac lobortis vel dapibus', 200, 6, 43, 2, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (248, 30, null, 532, 3, 24, 2, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (249, 87, null, 440, 5, 10, 3, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (250, 48, null, 925, 6, 22, 1, 8);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (251, 11, 'ultricies eu nibh quisque id justo sit amet', 720, 4, 13, 1, 8);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (252, 84, null, 251, 5, 29, 1, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (253, 49, 'lacinia sapien quis libero nullam sit amet', 783, 3, 42, 3, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (254, 90, null, 709, 3, 33, 1, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (255, 30, null, 228, 4, 23, 1, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (256, 0, null, 340, 5, 35, 1, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (257, 44, null, 364, 2, 49, 3, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (258, 98, null, 44, 3, 15, 3, 3);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (259, 62, 'aliquam quis turpis eget elit sodales scelerisque mauris sit amet', 648, 3, 32, 1, 9);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (260, 95, null, 982, 4, 2, 2, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (261, 61, null, 699, 1, 22, 1, 1);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (262, 30, null, 836, 4, 43, 1, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (263, 60, null, 899, 1, 37, 2, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (264, 48, null, 513, 2, 34, 2, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (265, 22, null, 703, 5, 1, 2, 9);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (266, 23, null, 323, 4, 29, 3, 15);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (267, 68, null, 508, 6, 24, 1, 2);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (268, 92, null, 936, 5, 45, 2, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (269, 38, null, 881, 3, 27, 1, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (270, 28, 'erat eros viverra eget congue eget semper rutrum nulla nunc', 755, 3, 38, 3, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (271, 69, null, 888, 4, 36, 2, 9);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (272, 50, null, 560, 3, 12, 3, 8);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (273, 38, null, 88, 1, 9, 1, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (274, 58, null, 654, 5, 42, 2, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (275, 46, 'natoque penatibus et magnis dis parturient', 904, 3, 52, 1, 7);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (276, 55, null, 459, 1, 30, 2, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (277, 28, null, 25, 6, 67, 1, 9);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (278, 36, null, 890, 6, 59, 2, 13);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (279, 71, null, 212, 1, 53, 1, 5);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (280, 42, null, 955, 4, 54, 2, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (281, 78, null, 948, 4, 22, 2, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (282, 90, null, 960, 1, 15, 3, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (283, 28, null, 720, 5, 69, 1, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (284, 54, null, 910, 2, 43, 1, 16);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (285, 71, null, 48, 2, 4, 1, 6);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (286, 91, null, 477, 4, 62, 3, 15);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (287, 58, null, 425, 6, 18, 2, 6);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (288, 10, null, 331, 6, 50, 1, 13);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (289, 30, null, 727, 2, 66, 1, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (290, 7, null, 640, 3, 25, 1, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (291, 75, null, 861, 1, 44, 2, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (292, 83, null, 909, 5, 40, 1, 4);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (293, 81, null, 625, 1, 8, 3, 3);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (294, 62, null, 517, 2, 54, 2, 10);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (295, 5, null, 870, 3, 40, 2, 11);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (296, 2, null, 867, 2, 59, 3, 12);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (297, 93, null, 230, 1, 45, 1, 3);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (298, 82, 'amet sapien dignissim vestibulum vestibulum ante', 724, 5, 32, 2, 17);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (299, 83, null, 299, 3, 34, 2, 7);
insert into SPECIFIKACE (ID_SPECIFIKACE, MNOZSTVI, POZNAMKA, CENA, ID_VELIKOST, ID_PRODUKT, ID_POHLAVI, ID_BARVA)
values (300, 71, 'tortor risus dapibus augue vel', 200, 6, 44, 3, 17);

commit;


--100 zaznamu adresa

insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (1, 'Massachusetts', 'Boston', '330 61', 'Warbler', 213, 33, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (2, 'Oklahoma', 'Edmond', '491 40', null, 130, null, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (3, 'Minnesota', 'Saint Paul', '021 60', 'Logan', 205, 31, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (4, 'California', 'Sacramento', '832 06', 'Lawn', 148, 75, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (5, 'District of Columbia', 'Washington', '627 33', 'Dwight', 216, 56, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (6, 'Alabama', 'Huntsville', '527 31', 'Lillian', 619, 24, 'Proin eu mi.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (7, 'West Virginia', 'Huntington', '769 54', 'Banding', 237, null, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (8, 'New York', 'New York City', '468 97', 'Cambridge', 251, 32, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (9, 'Alabama', 'Mobile', '686 97', 'Troy', 339, null,
        'Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (10, 'Texas', 'Amarillo', '028 78', null, 295, 24,
        'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (11, 'Illinois', 'Chicago', '072 10', 'Superior', 687, null,
        'In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (12, 'Florida', 'Punta Gorda', '400 07', null, 230, null, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (13, 'Texas', 'Dallas', '908 67', 'Fallview', 235, 57, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (14, 'Florida', 'Jacksonville', '852 48', 'Schmedeman', 284, 34, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (15, 'Oklahoma', 'Oklahoma City', '565 90', 'Pleasure', 755, null, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (16, 'Pennsylvania', 'Harrisburg', '905 48', 'Hauk', 914, 8, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (17, 'Missouri', 'Lees Summit', '428 26', 'Bashford', 500, 30, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (18, 'Florida', 'Tallahassee', '754 20', 'Del Sol', 405, 14, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (19, 'Alabama', 'Birmingham', '042 77', 'Helena', 777, 34,
        'Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (20, 'Kansas', 'Topeka', '858 88', 'Drewry', 709, 30, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (21, 'New York', 'Albany', '945 13', null, 265, 45,
        'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (22, 'California', 'Van Nuys', '028 48', null, 805, 36, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (23, 'Utah', 'Salt Lake City', '957 74', 'Old Gate', 790, 28, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (24, 'Arizona', 'Tucson', '179 57', 'Burrows', 272, null,
        'Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (25, 'Iowa', 'Des Moines', '392 86', 'Westend', 925, null, 'Curabitur gravida nisi at nibh.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (26, 'Missouri', 'Kansas City', '508 15', 'Elmside', 575, 58,
        'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (27, 'Connecticut', 'Hartford', '021 89', 'Arrowood', 250, null, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (28, 'South Carolina', 'Myrtle Beach', '816 90', 'Scofield', 306, 71, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (29, 'Florida', 'Miami', '883 20', null, 705, 50,
        'Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (30, 'Oregon', 'Salem', '611 87', null, 748, 64,
        'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (31, 'Massachusetts', 'Newton', '530 09', 'Summerview', 567, 6, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (32, 'Florida', 'Pensacola', '355 91', 'Fairview', 589, 75,
        'Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (33, 'Colorado', 'Englewood', '282 95', 'Nobel', 657, 9, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (34, 'California', 'Los Angeles', '066 44', 'Brickson Park', 999, 3,
        'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (35, 'Colorado', 'Denver', '531 13', null, 722, 96, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (36, 'Virginia', 'Lynchburg', '099 90', 'Esch', 758, 88, 'Morbi a ipsum. Integer a nibh. In quis justo.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (37, 'District of Columbia', 'Washington', '273 54', 'Sloan', 865, 32,
        'Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (38, 'Florida', 'Naples', '644 24', null, 231, 27, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (39, 'Florida', 'Lehigh Acres', '907 01', 'Florence', 647, 35, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (40, 'California', 'Orange', '257 04', 'Packers', 691, 29, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (41, 'Georgia', 'Atlanta', '139 87', 'Northfield', 934, 95, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (42, 'Texas', 'Austin', '659 13', 'Comanche', 638, 78,
        'Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (43, 'Arizona', 'Phoenix', '005 17', 'Fallview', 511, null, 'Aenean lectus. Pellentesque eget nunc.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (44, 'California', 'Sacramento', '257 69', 'West', 843, null,
        'Vivamus in felis eu sapien cursus vestibulum. Proin eu mi.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (45, 'Michigan', 'Detroit', '651 81', null, 714, 57,
        'Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (46, 'Iowa', 'Des Moines', '577 33', 'Tennyson', 166, null,
        'Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (47, 'Mississippi', 'Hattiesburg', '421 91', 'Chinook', 531, 35, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (48, 'West Virginia', 'Huntington', '953 63', null, 169, 48, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (49, 'Alabama', 'Birmingham', '691 85', 'Northwestern', 565, 41, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (50, 'Virginia', 'Herndon', '590 25', 'Magdeline', 839, 19,
        'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (51, 'Louisiana', 'Baton Rouge', '238 74', 'Sage', 351, null, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (52, 'Utah', 'Salt Lake City', '844 99', null, 603, 79,
        'Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (53, 'Alaska', 'Anchorage', '038 93', 'Reindahl', 188, null,
        'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (54, 'California', 'San Diego', '424 85', 'Vera', 510, null,
        'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (55, 'Florida', 'Miami', '329 86', null, 707, null,
        'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (56, 'California', 'Oceanside', '601 60', null, 515, 76,
        'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (57, 'Maryland', 'Baltimore', '273 55', null, 804, 75,
        'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (58, 'Kentucky', 'Louisville', '350 88', 'Katie', 218, 51, 'Vivamus tortor. Duis mattis egestas metus.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (59, 'California', 'Berkeley', '932 63', 'Namekagon', 993, null,
        'Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (60, 'Nebraska', 'Omaha', '622 96', null, 839, 41,
        'Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (61, 'Illinois', 'Springfield', '236 35', null, 586, 33, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (62, 'Mississippi', 'Meridian', '269 78', null, 935, null, 'Sed ante.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (63, 'Texas', 'Houston', '914 13', null, 139, 66, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (64, 'North Carolina', 'High Point', '097 89', 'Gerald', 805, 17, 'Nulla mollis molestie lorem.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (65, 'Michigan', 'Southfield', '566 02', 'Rutledge', 862, 33, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (66, 'California', 'Santa Monica', '547 99', 'Arkansas', 508, 18, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (67, 'Texas', 'Denton', '349 10', 'Talmadge', 431, 18,
        'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (68, 'Pennsylvania', 'Harrisburg', '190 25', 'Upham', 332, 68, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (69, 'California', 'Santa Monica', '198 97', null, 181, null, 'Aliquam erat volutpat.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (70, 'Massachusetts', 'Springfield', '020 38', 'Maryland', 759, 75, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (71, 'New York', 'Yonkers', '058 72', null, 314, 82, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (72, 'Kansas', 'Wichita', '644 52', 'Kropf', 163, 36, 'Aenean lectus.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (73, 'New Mexico', 'Santa Fe', '257 26', 'Sutteridge', 292, 41, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (74, 'California', 'Sacramento', '365 96', 'Riverside', 811, 68, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (75, 'Pennsylvania', 'Pittsburgh', '897 01', 'Montana', 671, 66,
        'Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (76, 'California', 'San Diego', '603 02', 'Monument', 528, 11, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (77, 'Florida', 'Miami', '369 18', 'Melvin', 238, 56,
        'Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (78, 'California', 'Glendale', '739 62', 'Westport', 153, 88, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (79, 'Maryland', 'Baltimore', '693 25', null, 848, 85,
        'Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (80, 'Virginia', 'Ashburn', '366 10', 'Bashford', 642, 79, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (81, 'Indiana', 'Evansville', '737 44', null, 460, 16, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (82, 'Michigan', 'Detroit', '380 87', null, 450, 13,
        'Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (83, 'Massachusetts', 'Worcester', '931 54', 'Clyde Gallagher', 293, 41, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (84, 'Florida', 'Tampa', '629 17', null, 505, null, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (85, 'Indiana', 'South Bend', '209 84', null, 520, null, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (86, 'North Carolina', 'Charlotte', '735 58', 'Merchant', 532, null, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (87, 'Oklahoma', 'Oklahoma City', '255 30', 'Westerfield', 909, null, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (88, 'Texas', 'Houston', '503 47', 'Colorado', 912, null,
        'Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (89, 'Texas', 'Houston', '173 87', 'Eggendart', 390, 25,
        'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (90, 'Georgia', 'Atlanta', '603 40', null, 839, null,
        'Quisque id justo sit amet sapien dignissim vestibulum.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (91, 'Illinois', 'Springfield', '698 21', 'Haas', 568, 35,
        'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (92, 'Florida', 'Pensacola', '052 56', 'Cascade', 690, 86,
        'Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (93, 'South Dakota', 'Sioux Falls', '775 60', 'Stone Corner', 518, 23,
        'In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat.');
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (94, 'Delaware', 'Wilmington', '232 25', 'Lake View', 173, 77, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (95, 'Georgia', 'Atlanta', '933 98', 'Fieldstone', 815, 47, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (96, 'Kansas', 'Shawnee Mission', '010 87', null, 918, 13, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (97, 'Nevada', 'Las Vegas', '963 68', 'Basil', 853, 97, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (98, 'Alabama', 'Montgomery', '071 29', null, 546, 70, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (99, 'Connecticut', 'Waterbury', '183 31', 'School', 741, 16, null);
insert into ADRESA (ID_ADRESA, STAT, MESTO, PSC, ULICE, CISLO_POPISNE, CISLO_ORIENTACNI, POZNAMKA)
values (100, 'California', 'San Francisco', '935 29', 'Graceland', 883, 40,
        'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy.');
commit;


--100 zaznamu o uzivatelich

insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (1, 'Jonie', 'Ragge', 'Dr', 'Mr', 'jragge0@nasa.gov', '0',
        '4b29fc3e9f084a00a74f502ca365de2049ab91d91e8c2d43c50baa0d2535702d', null, 3, 1, 5, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (2, 'Gwenore', 'Hallawell', 'Honorable', null, 'ghallawell1@mit.edu', '0',
        'ca2a8e680e6aca0819672febd56c5c9b7ce6452531998c252985a267791bdfe2', null, 3, 3, 2, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (3, 'Franciskus', 'Scrane', 'Mr', null, 'fscrane2@ted.com', '0',
        'a78d416fab062eccb9adfe045d3896d5155a379139ca603626b828cd42fc1866', '+420833075895', 2, 2, 11, 60);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (4, 'Sibelle', 'Brient', null, null, 'sbrient3@sciencedirect.com', '0',
        'c2c7a2b5266760a8060c2a73054c26edc7ecc2d3243ee78bbd48afa6682e6d26', '+420359033572', 2, 6, 41, 88);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (5, 'Eddi', 'Lere', 'Rev', null, 'elere4@elpais.com', '0',
        '36e39de4af53786d96b927197701778896383ad81ae7f56f7f38123d28fbd24c', '+420545343763', 1, 5, 22, 36);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (6, 'Tremain', 'Vickerstaff', 'Dr', null, 'tvickerstaff5@sciencedaily.com', '0',
        '4979b9b409bccf02e6ac6655aad0de37600e305eee43cc25d39f2ba6388cdace', null, 2, 3, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (7, 'Dougie', 'Eilhersen', 'Rev', null, 'deilhersen6@google.it', '0',
        '6b8bbf86c08f54fe5cfab50c5ac13999afa9770e52ea38b7add402cb54a949c6', null, 1, 2, 8, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (8, 'Corrina', 'Gozard', 'Mrs', null, 'cgozard7@tiny.cc', '0',
        '179666bda322d54426c122f71047f33bbac189ed6793afaf60ec811ee074e778', null, 1, 2, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (9, 'April', 'Pelham', 'Mr', null, 'apelham8@squidoo.com', '0',
        'a4da1e9f776a17b25d06968869d3c38d5f3d5de5415fb3e75b8b20e2fdbd041c', '+420274358226', 2, 6, 67, 4);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (10, 'Blane', 'Goadbie', 'Mr', null, 'bgoadbie9@cpanel.net', '0',
        'c730d83c3baa2143b155a42ceff04ee5f67c5a043ec9a5c8f731fbd37de1f56f', '+420567231441', 1, 2, 23, 67);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (11, 'Alanna', 'Kilday', 'Dr', null, 'akildaya@stanford.edu', '0',
        '51e641a1b2a7eb893a54839e5477368e22da0bb31bc1989f6afc6256094d12ac', null, 1, 5, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (12, 'Chrisy', 'Branigan', 'Mrs', null, 'cbraniganb@youtu.be', '0',
        '93626137a711ee23a7ff942f8af789686277a332780e8b40dbea6410918963ed', null, 2, 3, 20, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (13, 'Cate', 'Trail', 'Honorable', null, 'ctrailc@newyorker.com', '0',
        'abc02d799cb7e45fff5ce850bc68b968c543dea1b7d628faed0b1573af30d292', null, 2, 1, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (14, 'Janeczka', 'Bridgwater', null, null, 'jbridgwaterd@ca.gov', '0',
        '4cef2475ef80fd82a09cd81d31190cbc72017befb524a199f6075ddbb9b12d29', null, 2, 2, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (15, 'Clair', 'Gobel', null, 'Rev', 'cgobele@4shared.com', '0',
        '3a685b0c15855cc6e0e6c4190612e3893e14a46fb208b15bc357434d31e349ae', null, 2, 6, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (16, 'Eli', 'Carrel', 'Dr', null, 'ecarrelf@phoca.cz', '0',
        '9c23488db31ed82985daa71603741a7667038c19ef6a9628fc265801de3fd948', '+420716956417', 3, 6, 99, 64);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (17, 'Charlotta', 'Olivie', 'Mr', null, 'colivieg@symantec.com', '0',
        '75e508ea0bb6c4eed6286af31bce951969bf6f157585110a2542f3c179e4f3ca', null, 1, 5, 30, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (18, 'Antonio', 'Duddin', 'Dr', 'Rev', 'aduddinh@odnoklassniki.ru', '0',
        'd7f2772367c3d98c313b55b120ff0cdf3877c1776db92bc89a876319688487e3', null, 3, 5, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (19, 'Teddie', 'Gauden', 'Mrs', null, 'tgaudeni@mac.com', '0',
        '066e3c6b3ceea2c42cae0466badb9e0b78d765800726804e67cd7db066872a16', null, 3, 2, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (20, 'Tiphany', 'Bygrove', 'Ms', null, 'tbygrovej@bloglines.com', '0',
        '6afb3bd0b7f7136154d3024cc51018baf0203eb28c7a23cf3d91d521d57cac3b', '+420185243851', 1, 6, 43, 91);
--zde zacinaji overene emaily
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (21, 'Janetta', 'Addy', null, null, 'jaddy0@ca.gov', '1',
        '04080f1226696687d00807fb3dbadc1c59b7616c3823a347945ec2096b47f242', null, 1, 3, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (22, 'Garnet', 'Gandley', 'Mrs', null, 'ggandley1@godaddy.com', '1',
        '63340c20c778f73efe3803a3193649234e069c1b2db07485b99e24a079701733', null, 3, 1, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (23, 'Brooke', 'Mowsley', 'Ms', null, 'bmowsley2@discovery.com', '1',
        '3a69e61876ba50811cf95e1c821e88cc63a634862c13e636e3f7367c2bd55650', '+420050359560', 1, 6, 52, 61);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (24, 'Auroora', 'Maplethorpe', 'Ms', null, 'amaplethorpe3@mail.ru', '1',
        '993fdc35d5619134963f1ca3734801dacd390d70d3cef884dc71546c41b1f523', '+420735382796', 3, 6, 30, 46);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (25, 'Tawnya', 'Hymer', 'Mrs', null, 'thymer4@fema.gov', '1',
        '2b72c4750089ca26fbccd5f003700f3705afb15300efd43d89ce407f9790a5f2', null, 2, 3, 6, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (26, 'Wesley', 'Aronoff', null, null, 'waronoff5@cloudflare.com', '1',
        'd7b8fe1880c8185e8f00b55fe31e47e2d72cd33cb1b4cfdaab7e78d3e42f0b25', '+420869682877', 2, 6, 3, 37);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (27, 'Harrison', 'O''Skehan', 'Rev', null, 'hoskehan6@telegraph.co.uk', '1',
        '1861fd6f43ee94e5c702c6568789602c776ea0c1821a4a3c1e92a172251a4b33', null, 3, 2, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (28, 'Jennette', 'McNevin', 'Mr', null, 'jmcnevin7@phoca.cz', '1',
        '228704ee45eec0d3f127e6b6e2f53d832f72b03c0ada0f071c8d144f5e27c914', '+420830240443', 2, 1, 40, 73);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (29, 'Cameron', 'Markovic', null, null, 'cmarkovic8@smh.com.au', '1',
        '8db76950ada6c89fca7d6a0bc2402aae1bba7119e0a5adeae76ee304e3b94d3b', null, 3, 5, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (30, 'Gustie', 'Camerello', 'Mrs', null, 'gcamerello9@liveinternet.ru', '1',
        '57c2f74b26901d438de5310be9e553ef1411b079d0cc57f3b34151f86217d432', null, 2, 3, 15, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (31, 'Vernice', 'Gullifant', null, null, 'vgullifanta@wiley.com', '1',
        '9be2219c40cb6118e28ebe31acb288b7c05ebe778546aea8fb5aaefa84d71b04', null, 1, 5, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (32, 'Prentice', 'Hasluck', 'Rev', null, 'phasluckb@stanford.edu', '1',
        'a00e3d5c08bb6091c175c9741923a3efb668e398b989e54dfe74a9ba52c1966b', '+420408760853', 2, 2, 28, 90);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (33, 'Elliott', 'Welman', null, null, 'ewelmanc@ebay.com', '1',
        'a656c72917f31ff102615a0d9fac80c1ba99f8a177871bcc4042adf4445aaaa9', '+420052132117', 2, 4, 94, 16);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (34, 'Luther', 'Loomis', 'Honorable', 'Mr', 'lloomisd@google.com.au', '1',
        '0a2a0b01f277aa0a2bed4cc49bc3f1e0160587c41a33d21e0f1b0f5f6036dfe7', null, 2, 6, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (35, 'Cordy', 'Niesing', null, 'Dr', 'cniesinge@wired.com', '1',
        '2bbc941f37c73ff47ee2d90cdcda42681f32ab5795a088fd420e2a35381491a9', '+420462935368', 2, 4, 51, 47);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (36, 'Margaretha', 'Hallybone', 'Mr', null, 'mhallybonef@cyberchimps.com', '1',
        '44851ad000a58800edc00e6a4bbf8109473880a5db1bde42bfaf71e9b9865326', '+420001720492', 1, 5, 1, 9);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (37, 'Eva', 'Menicomb', null, 'Honorable', 'emenicombg@loc.gov', '1',
        'b965e4e3d8b9bfd864f9bacd455f9f524a82680ece5e5571b619b0c99260dd19', null, 3, 6, 13, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (38, 'Klara', 'Grosvener', null, null, 'kgrosvenerh@ftc.gov', '1',
        '673432650c191df43eb65975906ac8a0e45babf42e7e95a66d32e3e2752d4652', '+420216930860', 1, 5, 96, 24);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (39, 'Amalee', 'Skamell', 'Rev', null, 'askamelli@hud.gov', '1',
        'ed0d577744884fb2298591800309382fc369f01609a99366233a68293ee28c28', null, 1, 5, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (40, 'Jacynth', 'Jullian', 'Mrs', 'Mr', 'jjullianj@thetimes.co.uk', '1',
        '2f6b9c9d8de473a0f425eee3de74236f38a01146ab6a8898ab1a83e2ba87c385', null, 2, 2, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (41, 'Milli', 'Grinham', null, null, 'mgrinhamk@surveymonkey.com', '1',
        'c226d27447e460f3a0bf57a90751409cf3593781fe0140ce294cba4fc1a2b0cd', null, 3, 5, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (42, 'Olga', 'Milner', null, null, 'omilnerl@hibu.com', '1',
        '4a0a69d37a9fbe15654376aefbfda1dfb180a55f71e5e96351a33957bc51f357', null, 1, 2, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (43, 'Lavina', 'Awty', null, null, 'lawtym@soup.io', '1',
        'dbc04f8bbaede3b170f0812cca425d447a8984c4f80c0a1a7d7fb876c55d6394', null, 3, 2, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (44, 'Colby', 'Mc Andrew', 'Mrs', 'Honorable', 'cmcandrewn@ebay.com', '1',
        '35e2d3d8c6f5fc6868b8fe53fc05a00b629bf42d17f888b9c9d6de551e5d2556', '+420727486452', 1, 6, 65, 54);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (45, 'Jacobo', 'Mollene', 'Honorable', null, 'jmolleneo@wikia.com', '1',
        '51f40bfda0f124b581518bf46cada6b9729c3c7338c43338aa76cb3999b816e7', '+420010950072', 1, 3, 5, 68);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (46, 'Brenn', 'Blay', null, null, 'bblayp@about.me', '1',
        '5d5041a3ff142672f3577404449a4e351b8d616ab4b40056365ccddf192fa6ab', '+420509340284', 3, 5, 34, 58);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (47, 'Erinn', 'Lilford', null, null, 'elilfordq@alibaba.com', '1',
        '6d7b1d098d0c2e3e79566498a56d3d5788bcd0577308d07b167b33599df7f18b', null, 2, 3, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (48, 'Maggee', 'Redhouse', 'Dr', null, 'mredhouser@samsung.com', '1',
        '92cf1fb70713f6a76bb2d94241840c03364cc62a8ad8e833abbfd9e68622c519', '+420441026689', 2, 6, 66, 96);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (49, 'Say', 'D''Agostino', 'Dr', null, 'sdagostinos@yelp.com', '1',
        'c196f4bae0545dbc4900a0c3ace13d2fdf12471af969061b6fc983798a92ac64', '+420887606575', 3, 3, 91, 24);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (50, 'Patric', 'Garrard', 'Honorable', null, 'pgarrardt@geocities.com', '1',
        'f9d5154607e04643625b7032bd48cd7392fff345276723e97c958dc8f085a815', '+420733049520', 3, 6, 80, 6);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (51, 'Jill', 'Heaker', 'Ms', null, 'jheakeru@booking.com', '1',
        'b36fd2b1e00cae7003395132f69548e55916dfea130f1fe7639dd7d4a751c721', null, 1, 5, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (52, 'Kylila', 'Waszczyk', 'Ms', null, 'kwaszczykv@skyrock.com', '1',
        'f7298cd9fbe22cb7943d9a8afda6176dd4c132fde1e5e7db04a7331651c97e85', null, 2, 2, 20, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (53, 'Odele', 'Devey', null, 'Dr', 'odeveyw@yahoo.co.jp', '1',
        '843ff03ae6f85ec540de9e60a2e9e9525d66d12d0d510ab94525b089783dec47', '+420567859188', 2, 3, 40, 19);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (54, 'Chrisy', 'Jerzak', null, 'Rev', 'cjerzakx@google.com.br', '1',
        '64565bb02fd7bd645e471ff56a9145f7ce893d92266707edca3f83737900658d', null, 3, 5, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (55, 'Cosme', 'Thripp', 'Mr', null, 'cthrippy@microsoft.com', '1',
        'b4040ad9a3c64a4b2d393ae88d463a3856715828e4239ad4d52ee58067f00570', null, 2, 4, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (56, 'Renaldo', 'Brandrick', 'Ms', 'Mr', 'rbrandrickz@theguardian.com', '1',
        '58343cc9b1115253dbe6730018382f921dc093225f9a3428dfe30eacb2fb2c68', '+420911209038', 1, 6, 91, 92);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (57, 'Dory', 'Melendez', 'Ms', 'Mr', 'dmelendez10@last.fm', '1',
        '7c359f43b31fa87e56ad95969ab0ea29be1ed09d34b3283f31a4b89a356685f3', '+420690405615', 3, 4, 7, 18);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (58, 'Harbert', 'Grigsby', 'Rev', 'Rev', 'hgrigsby11@shinystat.com', '1',
        'c6227be2c7255a8c80146a8eb56b21cb928456f32c6abc604aeefe0899be5a03', null, 3, 3, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (59, 'Clareta', 'Garlic', 'Mr', null, 'cgarlic12@arstechnica.com', '1',
        'd41ef23b2594a45ab26c8a4a58c61e7929e1bd3c3d0e25ffdef88020ecefe0ed', '+420408305114', 3, 6, 2, 38);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (60, 'Lanny', 'Tapin', 'Ms', null, 'ltapin13@hp.com', '1',
        '4a8756f054b6612e7de3318faad1f903d2afb8ea41188f1d015647b8f7775fb4', null, 2, 6, 89, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (61, 'Tera', 'Billingsley', 'Honorable', null, 'tbillingsley14@illinois.edu', '1',
        '7423eeb7084ccc8dbf00d25834228a375d56da14d76bbd06c8960f2becbbc425', null, 3, 4, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (62, 'Gratiana', 'Brownsett', 'Ms', null, 'gbrownsett15@psu.edu', '1',
        '99b9391686ec19d0b5568fd97f7cbf0edec9832eb0fb54b5de2afe632a8fcba5', null, 3, 5, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (63, 'Diego', 'Enriques', 'Mrs', null, 'denriques16@cam.ac.uk', '1',
        '705a669847fefe8a336f8b93890fef895e9206093818ba38bbd05fc15fc8ce90', '+420439408367', 3, 3, 20, 97);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (64, 'Leisha', 'Dorant', 'Honorable', 'Mr', 'ldorant17@skyrock.com', '1',
        'b2ff816cc43a4a33a34d2eee798939ae31dc4ccba42bf17ae59f4151fb1836c7', '+420710277885', 3, 2, 95, 35);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (65, 'Aridatha', 'Few', 'Mr', null, 'afew18@phpbb.com', '1',
        'b894e2721f1c5e4aae2960214650d6378b9780a582183ae8ce1461b9c3126dee', null, 3, 3, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (66, 'Jenni', 'Padrick', null, 'Rev', 'jpadrick19@smugmug.com', '1',
        'c251ae7a44e5119f2567062c7d5c12440c68545bfb1bc021b7cf6e930cf188be', '+420634035987', 1, 2, 4, 62);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (67, 'Brewer', 'Paliser', 'Mrs', null, 'bpaliser1a@telegraph.co.uk', '1',
        'a1840aa108c704aedb62dc9e3d15ba89c6dff58ae26a613b1258d21037b0ee1a', null, 3, 2, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (68, 'Christi', 'Strutz', null, null, 'cstrutz1b@mac.com', '1',
        '691ab33ee826845e3b62eca18dbd5274fcc4b98663540e42a3f38cbc28f278ab', null, 3, 6, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (69, 'Cecil', 'Featley', 'Rev', null, 'cfeatley1c@yahoo.co.jp', '1',
        '656074721c1d077a4f4cfe30c0d074642722707806df4feb2a8d58c7e26fe659', '+420976403904', 1, 2, 96, 81);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (70, 'Tybi', 'Woodger', null, null, 'twoodger1d@rambler.ru', '1',
        '3a1957af3dbf5201b778ce58180c1f7ef2cb965b309cb8e39766753b8f231f7c', null, 3, 5, 56, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (71, 'Mendie', 'Reynolds', 'Dr', null, 'mreynolds1e@msu.edu', '1',
        '3bdde042e8994317d5d4890f9788316b93db9d9cfde907e1cabf998421458ac2', '+420802136804', 1, 4, 16, 30);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (72, 'Alf', 'Krelle', 'Dr', null, 'akrelle1f@ibm.com', '1',
        'df230b4025ff333138646715f896eb67d57fe569662672bfbc89ba9fbf3f428f', '+420302631909', 3, 5, 37, 77);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (73, 'Errol', 'Talby', 'Rev', null, 'etalby1g@topsy.com', '1',
        'c5495414bfb5638dc805573dac1b9ca9ed0ce46a9da78f37b68f8a7c7f531066', null, 1, 4, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (74, 'Lorettalorna', 'Clementi', 'Mrs', null, 'lclementi1h@jiathis.com', '1',
        '04884d32cc26a1a682cf19a1047dc5cb6347fa2d7ab78f6ccadeff23d31dfaaa', '+420272425284', 3, 1, 85, 55);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (75, 'Serge', 'Filippozzi', 'Mr', 'Mrs', 'sfilippozzi1i@ucoz.ru', '1',
        '232b117e4c15b0b8a83d03b99610149317ee733a25aa03f6fc2bf4aee4d6541e', '+420799031832', 2, 1, 79, 78);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (76, 'Deanne', 'Ramsbottom', 'Rev', null, 'dramsbottom1j@163.com', '1',
        '58547b97be89fd36fe060b074a240bb2a283f18bf96dab30e75f56047897c474', null, 2, 5, 56, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (77, 'Wrennie', 'Snalom', 'Dr', null, 'wsnalom1k@loc.gov', '1',
        '69906050f2a8ddfd7ea1240d18dbb790765a1e41a5465ada04e9277e0038d5b1', null, 1, 5, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (78, 'Lolita', 'Shawe', 'Mr', 'Dr', 'lshawe1l@sogou.com', '1',
        'e462754ed0a970302d72a1e0b0045f774bc3677c25631bda47f53edb5a8b1d71', '+420734608699', 3, 2, 20, 81);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (79, 'Delcine', 'Golightly', 'Mrs', null, 'dgolightly1m@psu.edu', '1',
        '68411301474d1c4034edf5abf1a6c36afbc26e28b23c3dd186bc310b50bc36cd', '+420565229293', 2, 1, 86, 13);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (80, 'Lyle', 'Westhoff', 'Mr', null, 'lwesthoff1n@posterous.com', '1',
        '874c98b0184c9c1576bb2cf4b121990b9f3aa0a02102f0dd10afb6512f10814f', '+420043783106', 2, 4, 27, 39);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (81, 'Elton', 'Bonifas', 'Rev', null, 'ebonifas1o@bloglines.com', '1',
        '45585a0460eaeaefe5e95b2536ddf963e7789cb9385a88e0c04a0893fed378bf', '+420887210340', 3, 6, 70, 26);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (82, 'Britney', 'Leng', 'Ms', null, 'bleng1p@flickr.com', '1',
        '2b286257e54866164f80ac78eba1f39e262284ab5bebc46882ebfda93f895c66', null, 2, 6, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (83, 'Louisa', 'Millins', 'Dr', null, 'lmillins1q@cdbaby.com', '1',
        '4cab0d44390d9942144289bd113d0fca4c60106c086a0ee18285d5c6ced4221a', null, 1, 6, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (84, 'Kendal', 'Stoakes', 'Mrs', null, 'kstoakes1r@jalbum.net', '1',
        '954ffe3bdeacd45b63a26bfe5b3ff4dbdab3bb9c0848ec3e60892bd4829fd5b7', '+420872404854', 2, 1, 70, 86);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (85, 'Michell', 'Sibille', 'Dr', null, 'msibille1s@google.com.br', '1',
        '0ee09eabf687919eee1deb8a44235b1b0fc3a443dfc9a8ede015dc85ee99baf5', null, 2, 6, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (86, 'Pauly', 'Needs', 'Rev', 'Mrs', 'pneeds1t@msn.com', '1',
        '2374b6eb1c805b92917c713da6d730061c9a9822e5e0cdd66b6dcae5f5e159df', '+420954671535', 3, 3, 61, 76);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (87, 'Terri', 'Gales', null, 'Mr', 'tgales1u@twitpic.com', '1',
        'f146fc1a6b1098b9bffc78df93319c6b02de3c3b1302eddb1a8119245e483a77', '+420065542882', 3, 5, 66, 6);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (88, 'Justina', 'Chesworth', 'Rev', null, 'jchesworth1v@utexas.edu', '1',
        '409eec6bab223ca6e22955138446935061417fae22c79662bd07b5112c3d6f26', null, 3, 5, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (89, 'Rock', 'Aleksidze', 'Dr', null, 'raleksidze1w@hostgator.com', '1',
        '22a2a7801f927d4c3cec54e9d55d92d6954b21bee27f559edde9649b522af7b3', '+420929663239', 3, 4, 63, 37);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (90, 'Brooke', 'Betho', null, null, 'bbetho1x@netscape.com', '1',
        '37f4c528cef6877ab84fd9f3d0e168058d1499dede97c06424a33979cb88c33d', null, 2, 1, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (91, 'Rodolphe', 'Dowsey', 'Rev', 'Dr', 'rdowsey1y@latimes.com', '1',
        '0cb0954099279b289d71b7e41106a88797db508da9b1c03f119c724a20d8f17e', null, 1, 6, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (92, 'Cloris', 'Tschiersch', 'Mrs', null, 'ctschiersch1z@psu.edu', '1',
        'd14efecccdd1cfe1d4a6ee837961796f48d488f91248341d10bc9e370ad38cfc', null, 2, 5, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (93, 'Odey', 'Burge', null, null, 'oburge20@unesco.org', '1',
        '11d02a6403af18e32f7f89a40f45ec968af1a349b31308e3d03f52b74a7dcc08', null, 1, 2, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (94, 'Evey', 'Ben', null, null, 'eben21@ning.com', '1',
        '501323952278f41dff7e13e98ae2ff221ed35b891cecf31d5e2480855f2feb96', '+420805702634', 3, 5, 65, 23);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (95, 'Ephrem', 'Greetham', 'Ms', 'Dr', 'egreetham22@storify.com', '1',
        'fda0d8301835bd383423d6fa61b854e23c506fa81f7370ecd2db3592effa5a28', null, 1, 4, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (96, 'Alicia', 'Errichiello', 'Honorable', null, 'aerrichiello23@123-reg.co.uk', '1',
        'a9c7cac78c519f20474780c4608bd24dc81b296f2dd6d77e621bd26541650b19', '+420720274504', 3, 2, 35, 23);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (97, 'Nathalie', 'Lergan', null, null, 'nlergan24@amazonaws.com', '1',
        'bfb8e4099030583705e6bdebc63a627eb23375b3553000c0d67317b09ad8d20c', '+420157723932', 3, 3, 17, 45);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (98, 'Cicely', 'Hebblewaite', 'Rev', 'Ms', 'chebblewaite25@mediafire.com', '1',
        '9f4d7ff139b18847f8f7618124044e8102c7b2200360c22a7da24b7f24462416', null, 2, 1, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (99, 'Magdaia', 'Stede', 'Honorable', null, 'mstede26@cbsnews.com', '1',
        '0c8695004365f8f9f8096588ef9190afbd24988874fc1280264a13c5c4a9c51c', null, 1, 5, null, null);
insert into UZIVATEL (ID_UZIVATEL, JMENO, PRIJMENI, TITUL_PRED_JMENEM, TITUL_ZA_JMENEM, EMAIL, OVERENO_EMAIL,
                      HESLO_HASH, TELEFON, ID_POHLAVI, ID_VELIKOST, ID_ADRESA, ID_ADRESA2)
values (100, 'Willow', 'Michell', 'Mr', 'Mrs', 'wmichell27@statcounter.com', '1',
        '0d90856406c26505c36d6e10d819add4f7a593ff6fc306d9dc134ce94feed49e', '+420723974714', 1, 4, 10, 65);
commit;


--300 zaznamu objednavka

insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (1, to_date('30-Mar-20', 'DD-MON-RR'), to_date('30-Apr-20', 'DD-MON-RR'), '0', 652.77, 13, 85, '0',
        'luctus.txt', 'Massachusetts', 'Boston', '330 61', 'Warbler', 213, 33, 'Massachusetts', 'Boston', '330 61',
        'Warbler', 213, 33, null, 79);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (2, to_date('19-Feb-20', 'DD-MON-RR'), to_date('19-Mar-20', 'DD-MON-RR'), '0', 863.57, 19, 92, '0',
        'integer.txt', 'Oklahoma', 'Edmond', '491 40', null, 130, null, 'Oklahoma', 'Edmond', '491 40', null, 130,
        null, null, 5);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (3, to_date('01-Jan-20', 'DD-MON-RR'), to_date('01-Feb-20', 'DD-MON-RR'), '0', 651.38, 13, 99, '1',
        'erat_tortor.txt', 'Minnesota', 'Saint Paul', '021 60', 'Logan', 205, 31, 'Minnesota', 'Saint Paul',
        '021 60', 'Logan', 205, 31, null, 59);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (4, to_date('20-Mar-20', 'DD-MON-RR'), to_date('20-Apr-20', 'DD-MON-RR'), '1', 710.57, 10, 52, '0',
        'sed.txt', 'California', 'Sacramento', '832 06', 'Lawn', 148, 75, 'California', 'Sacramento', '832 06',
        'Lawn', 148, 75, null, 23);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (5, to_date('20-Jan-20', 'DD-MON-RR'), to_date('20-Feb-20', 'DD-MON-RR'), '1', 644.89, 15, 113, '0',
        'semper_est.txt', 'District of Columbia', 'Washington', '627 33', 'Dwight', 216, 56, 'District of Columbia',
        'Washington', '627 33', 'Dwight', 216, 56, null, 73);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (6, to_date('20-Jan-20', 'DD-MON-RR'), to_date('20-Feb-20', 'DD-MON-RR'), '1', 689.25, 19, 115, '0',
        'libero_nam.txt', 'Alabama', 'Huntsville', '527 31', 'Lillian', 619, 24, 'Alabama', 'Huntsville', '527 31',
        'Lillian', 619, 24, null, 72);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (7, to_date('04-Apr-20', 'DD-MON-RR'), null, '1', 615.63, 14, 64, '0', 'imperdiet_sapien.txt',
        'West Virginia', 'Huntington', '769 54', 'Banding', 237, null, 'West Virginia', 'Huntington', '769 54',
        'Banding', 237, null, null, 15);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (8, to_date('26-Mar-20', 'DD-MON-RR'), to_date('26-Apr-20', 'DD-MON-RR'), '0', 519.08, 15, 107, '0',
        'curae_donec.txt', 'New York', 'New York City', '468 97', 'Cambridge', 251, 32, 'New York', 'New York City',
        '468 97', 'Cambridge', 251, 32, null, 81);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (9, to_date('09-Apr-20', 'DD-MON-RR'), null, '1', 583.72, 16, 76, '1', 'nullam_molestie_nibh.txt', 'Alabama',
        'Mobile', '686 97', 'Troy', 339, null, 'Alabama', 'Mobile', '686 97', 'Troy', 339, null, null, 18);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (10, to_date('26-Feb-20', 'DD-MON-RR'), to_date('26-Mar-20', 'DD-MON-RR'), '0', 545.42, 12, 100, '1',
        'nunc_rhoncus_dui.txt', 'Texas', 'Amarillo', '028 78', null, 295, 24, 'Texas', 'Amarillo', '028 78', null,
        295, 24, null, 46);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (11, to_date('20-Feb-20', 'DD-MON-RR'), to_date('20-Mar-20', 'DD-MON-RR'), '1', 418.87, 12, 76, '0',
        'accumsan.txt', 'Illinois', 'Chicago', '072 10', 'Superior', 687, null, 'Illinois', 'Chicago', '072 10',
        'Superior', 687, null, null, 12);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (12, to_date('07-Apr-20', 'DD-MON-RR'), null, '1', 580.35, 15, 94, '1', 'eu_felis_fusce.txt', 'Florida',
        'Punta Gorda', '400 07', null, 230, null, 'Florida', 'Punta Gorda', '400 07', null, 230, null, null, 62);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (13, to_date('26-Jan-20', 'DD-MON-RR'), to_date('26-Feb-20', 'DD-MON-RR'), '1', 515.65, 14, 102, '0',
        'volutpat.txt', 'Texas', 'Dallas', '908 67', 'Fallview', 235, 57, 'Texas', 'Dallas', '908 67', 'Fallview',
        235, 57, null, 82);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (14, to_date('29-Jan-20', 'DD-MON-RR'), to_date('29-Feb-20', 'DD-MON-RR'), '1', 922.83, 19, 101, '1',
        'est_donec.txt', 'Florida', 'Jacksonville', '852 48', 'Schmedeman', 284, 34, 'Missouri', 'Lees Summit',
        '428 26', 'Bashford', 500, 30, null, 42);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (15, to_date('30-Mar-20', 'DD-MON-RR'), to_date('30-Apr-20', 'DD-MON-RR'), '0', 289.55, 19, 111, '0',
        'praesent.txt', 'Oklahoma', 'Oklahoma City', '565 90', 'Pleasure', 755, null, 'Florida', 'Jacksonville',
        '852 48', 'Schmedeman', 284, 34,
        'ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra', 90);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (16, to_date('06-Apr-20', 'DD-MON-RR'), null, '1', 661.98, 19, 109, '0', 'amet.txt', 'Pennsylvania',
        'Harrisburg', '905 48', 'Hauk', 914, 8, 'Oklahoma', 'Oklahoma City', '565 90', 'Pleasure', 755, null, null,
        79);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (17, to_date('03-Jan-20', 'DD-MON-RR'), to_date('03-Feb-20', 'DD-MON-RR'), '1', 637.6, 17, 88, '0',
        'curae_duis.txt', 'Missouri', 'Lees Summit', '428 26', 'Bashford', 500, 30, 'Pennsylvania', 'Harrisburg',
        '905 48', 'Hauk', 914, 8, null, 20);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (18, to_date('21-Jan-20', 'DD-MON-RR'), to_date('21-Feb-20', 'DD-MON-RR'), '0', 758.86, 15, 84, '1',
        'nunc_viverra.txt', 'Florida', 'Tallahassee', '754 20', 'Del Sol', 405, 14, 'Alabama', 'Birmingham',
        '042 77', 'Helena', 777, 34, null, 80);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (19, to_date('22-Mar-20', 'DD-MON-RR'), to_date('22-Apr-20', 'DD-MON-RR'), '0', 671.79, 17, 92, '0',
        'quam.txt', 'Alabama', 'Birmingham', '042 77', 'Helena', 777, 34, 'Kansas', 'Topeka', '858 88', 'Drewry',
        709, 30, null, 27);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (20, to_date('16-Apr-20', 'DD-MON-RR'), null, '1', 477.3, 12, 60, '0', 'nibh_in_lectus.txt', 'Kansas',
        'Topeka', '858 88', 'Drewry', 709, 30, 'New York', 'Albany', '945 13', null, 265, 45, null, 56);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (21, to_date('09-Apr-20', 'DD-MON-RR'), null, '1', 448.72, 16, 115, '1', 'a_ipsum.txt', 'New York', 'Albany',
        '945 13', null, 265, 45, 'California', 'Van Nuys', '028 48', null, 805, 36, null, 11);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (22, to_date('18-Apr-20', 'DD-MON-RR'), null, '1', 461.82, 10, 63, '0', 'blandit_non_interdum.txt',
        'California', 'Van Nuys', '028 48', null, 805, 36, 'Utah', 'Salt Lake City', '957 74', 'Old Gate', 790, 28,
        null, 42);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (23, to_date('19-Apr-20', 'DD-MON-RR'), null, '1', 759.39, 20, 93, '0', 'in_lectus_pellentesque.txt', 'Utah',
        'Salt Lake City', '957 74', 'Old Gate', 790, 28, 'Arizona', 'Tucson', '179 57', 'Burrows', 272, null, null,
        93);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (24, to_date('11-Feb-20', 'DD-MON-RR'), to_date('11-Mar-20', 'DD-MON-RR'), '0', 469.0, 14, 92, '1',
        'lacus_curabitur_at.txt', 'Arizona', 'Tucson', '179 57', 'Burrows', 272, null, 'Iowa', 'Des Moines',
        '392 86', 'Westend', 925, null, null, 73);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (25, to_date('17-Jan-20', 'DD-MON-RR'), to_date('17-Feb-20', 'DD-MON-RR'), '1', 811.83, 13, 55, '0',
        'blandit_nam_nulla.txt', 'Iowa', 'Des Moines', '392 86', 'Westend', 925, null, 'Missouri', 'Kansas City',
        '508 15', 'Elmside', 575, 58, null, 44);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (26, to_date('27-Apr-20', 'DD-MON-RR'), null, '0', 443.94, 12, 84, '1', 'erat_eros.txt', 'Missouri',
        'Kansas City', '508 15', 'Elmside', 575, 58, 'Connecticut', 'Hartford', '021 89', 'Arrowood', 250, null,
        null, 93);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (27, to_date('19-Mar-20', 'DD-MON-RR'), to_date('19-Apr-20', 'DD-MON-RR'), '0', 783.36, 13, 96, '1',
        'praesent_blandit_lacinia.txt', 'Connecticut', 'Hartford', '021 89', 'Arrowood', 250, null,
        'South Carolina', 'Myrtle Beach', '816 90', 'Scofield', 306, 71, null, 58);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (28, to_date('07-Feb-20', 'DD-MON-RR'), to_date('07-Mar-20', 'DD-MON-RR'), '0', 418.53, 19, 81, '1',
        'amet_turpis_elementum.txt', 'South Carolina', 'Myrtle Beach', '816 90', 'Scofield', 306, 71, 'Florida',
        'Miami', '883 20', null, 705, 50, null, 7);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (29, to_date('24-Feb-20', 'DD-MON-RR'), to_date('24-Mar-20', 'DD-MON-RR'), '1', 511.45, 20, 111, '0',
        'porttitor_lorem_id.txt', 'Florida', 'Miami', '883 20', null, 705, 50, 'Oregon', 'Salem', '611 87', null,
        748, 64, null, 31);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (30, to_date('13-Feb-20', 'DD-MON-RR'), to_date('13-Mar-20', 'DD-MON-RR'), '0', 400.74, 18, 117, '0',
        'dictumst_morbi_vestibulum.txt', 'Oregon', 'Salem', '611 87', null, 748, 64, 'Massachusetts', 'Newton',
        '530 09', 'Summerview', 567, 6, null, 38);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (31, to_date('17-Feb-20', 'DD-MON-RR'), to_date('17-Mar-20', 'DD-MON-RR'), '1', 447.32, 12, 54, '0',
        'nulla.txt', 'Massachusetts', 'Newton', '530 09', 'Summerview', 567, 6, 'Florida', 'Tallahassee', '754 20',
        'Del Sol', 405, 14,
        'nunc nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede', 2);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (32, to_date('16-Feb-20', 'DD-MON-RR'), to_date('16-Mar-20', 'DD-MON-RR'), '1', 424.81, 12, 64, '1',
        'nisl_duis_bibendum.txt', 'Florida', 'Pensacola', '355 91', 'Fairview', 589, 75, 'Florida', 'Pensacola',
        '355 91', 'Fairview', 589, 75, null, 52);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (33, to_date('20-Apr-20', 'DD-MON-RR'), null, '0', 647.89, 14, 74, '1', 'cras.txt', 'Colorado', 'Englewood',
        '282 95', 'Nobel', 657, 9, 'California', 'Los Angeles', '066 44', 'Brickson Park', 999, 3, null, 15);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (34, to_date('29-Feb-20', 'DD-MON-RR'), to_date('29-Mar-20', 'DD-MON-RR'), '1', 638.83, 18, 67, '1',
        'blandit.txt', 'California', 'Los Angeles', '066 44', 'Brickson Park', 999, 3, 'Colorado', 'Denver',
        '531 13', null, 722, 96, null, 60);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (35, to_date('01-Apr-20', 'DD-MON-RR'), null, '0', 555.18, 14, 83, '0', 'interdum.txt', 'Colorado', 'Denver',
        '531 13', null, 722, 96, 'Virginia', 'Lynchburg', '099 90', 'Esch', 758, 88, null, 57);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (36, to_date('04-Jan-20', 'DD-MON-RR'), to_date('04-Feb-20', 'DD-MON-RR'), '1', 233.72, 15, 59, '0',
        'maecenas.txt', 'Virginia', 'Lynchburg', '099 90', 'Esch', 758, 88, 'District of Columbia', 'Washington',
        '273 54', 'Sloan', 865, 32, null, 40);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (37, to_date('06-Jan-20', 'DD-MON-RR'), to_date('06-Feb-20', 'DD-MON-RR'), '1', 489.31, 14, 84, '0',
        'ante_nulla.txt', 'District of Columbia', 'Washington', '273 54', 'Sloan', 865, 32, 'Florida', 'Naples',
        '644 24', null, 231, 27, null, 53);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (38, to_date('27-Apr-20', 'DD-MON-RR'), null, '0', 649.69, 11, 58, '1', 'eu_pede.txt', 'Florida', 'Naples',
        '644 24', null, 231, 27, 'Colorado', 'Englewood', '282 95', 'Nobel', 657, 9, null, 74);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (39, to_date('06-Jan-20', 'DD-MON-RR'), to_date('06-Feb-20', 'DD-MON-RR'), '0', 767.61, 19, 97, '0',
        'proin.txt', 'Florida', 'Lehigh Acres', '907 01', 'Florence', 647, 35, 'California', 'Orange', '257 04',
        'Packers', 691, 29, null, 53);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (40, to_date('09-Mar-20', 'DD-MON-RR'), to_date('09-Apr-20', 'DD-MON-RR'), '1', 762.92, 18, 101, '1',
        'id_nulla_ultrices.txt', 'California', 'Orange', '257 04', 'Packers', 691, 29, 'Georgia', 'Atlanta',
        '139 87', 'Northfield', 934, 95, 'morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec',
        59);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (41, to_date('28-Jan-20', 'DD-MON-RR'), to_date('28-Feb-20', 'DD-MON-RR'), '0', 739.55, 16, 113, '1',
        'nibh.txt', 'Georgia', 'Atlanta', '139 87', 'Northfield', 934, 95, 'Texas', 'Austin', '659 13', 'Comanche',
        638, 78, 'condimentum curabitur in libero ut massa volutpat convallis morbi odio odio', 1);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (42, to_date('11-Feb-20', 'DD-MON-RR'), to_date('11-Mar-20', 'DD-MON-RR'), '0', 954.22, 15, 70, '1',
        'rutrum.txt', 'Texas', 'Austin', '659 13', 'Comanche', 638, 78, 'Florida', 'Lehigh Acres', '907 01',
        'Florence', 647, 35,
        'posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in',
        59);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (43, to_date('03-Jan-20', 'DD-MON-RR'), to_date('03-Feb-20', 'DD-MON-RR'), '1', 775.99, 15, 95, '1',
        'sem_mauris.txt', 'Arizona', 'Phoenix', '005 17', 'Fallview', 511, null, 'California', 'Sacramento',
        '257 69', 'West', 843, null, null, 63);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (44, to_date('24-Apr-20', 'DD-MON-RR'), null, '1', 978.19, 11, 60, '1', 'ipsum.txt', 'California',
        'Sacramento', '257 69', 'West', 843, null, 'Michigan', 'Detroit', '651 81', null, 714, 57, null, 14);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (45, to_date('27-Feb-20', 'DD-MON-RR'), to_date('27-Mar-20', 'DD-MON-RR'), '0', 637.91, 20, 95, '0',
        'mauris_vulputate.txt', 'Michigan', 'Detroit', '651 81', null, 714, 57, 'Iowa', 'Des Moines', '577 33',
        'Tennyson', 166, null, null, 76);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (46, to_date('03-Mar-20', 'DD-MON-RR'), to_date('03-Apr-20', 'DD-MON-RR'), '1', 569.55, 16, 68, '1',
        'sodales.txt', 'Iowa', 'Des Moines', '577 33', 'Tennyson', 166, null, 'Mississippi', 'Hattiesburg',
        '421 91', 'Chinook', 531, 35, null, 41);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (47, to_date('13-Mar-20', 'DD-MON-RR'), to_date('13-Apr-20', 'DD-MON-RR'), '0', 741.34, 16, 65, '0',
        'euismod_scelerisque.txt', 'Mississippi', 'Hattiesburg', '421 91', 'Chinook', 531, 35, 'West Virginia',
        'Huntington', '953 63', null, 169, 48, null, 83);

insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (49, to_date('23-Apr-20', 'DD-MON-RR'), null, '0', 328.06, 12, 98, '1', 'rutrum_nulla.txt', 'Alabama',
        'Birmingham', '691 85', 'Northwestern', 565, 41, 'Virginia', 'Herndon', '590 25', 'Magdeline', 839, 19,
        null, 86);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (50, to_date('26-Feb-20', 'DD-MON-RR'), to_date('26-Mar-20', 'DD-MON-RR'), '1', 267.99, 18, 108, '1',
        'turpis_eget.txt', 'Virginia', 'Herndon', '590 25', 'Magdeline', 839, 19, 'Louisiana', 'Baton Rouge',
        '238 74', 'Sage', 351, null, null, 24);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (51, to_date('06-Mar-20', 'DD-MON-RR'), to_date('06-Apr-20', 'DD-MON-RR'), '0', 439.08, 15, 81, '0',
        'nullam_molestie.txt', 'Louisiana', 'Baton Rouge', '238 74', 'Sage', 351, null, 'Utah', 'Salt Lake City',
        '844 99', null, 603, 79, null, 7);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (52, to_date('10-Apr-20', 'DD-MON-RR'), null, '1', 309.43, 12, 68, '0', 'fusce_congue_diam.txt', 'Utah',
        'Salt Lake City', '844 99', null, 603, 79, 'Alaska', 'Anchorage', '038 93', 'Reindahl', 188, null, null,
        74);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (53, to_date('11-Jan-20', 'DD-MON-RR'), to_date('11-Feb-20', 'DD-MON-RR'), '1', 547.64, 16, 82, '1',
        'faucibus_orci.txt', 'Alaska', 'Anchorage', '038 93', 'Reindahl', 188, null, 'California', 'San Diego',
        '424 85', 'Vera', 510, null, null, 57);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (54, to_date('07-Jan-20', 'DD-MON-RR'), to_date('07-Feb-20', 'DD-MON-RR'), '0', 311.23, 14, 105, '1',
        'molestie_lorem.txt', 'California', 'San Diego', '424 85', 'Vera', 510, null, 'Florida', 'Miami', '329 86',
        null, 707, null, null, 32);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (55, to_date('01-Jan-20', 'DD-MON-RR'), to_date('01-Feb-20', 'DD-MON-RR'), '1', 370.18, 12, 63, '1',
        'ac_nibh_fusce.txt', 'Florida', 'Miami', '329 86', null, 707, null, 'California', 'Oceanside', '601 60',
        null, 515, 76, null, 85);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (56, to_date('14-Apr-20', 'DD-MON-RR'), null, '1', 435.16, 17, 99, '0', 'at.txt', 'California', 'Oceanside',
        '601 60', null, 515, 76, 'Arizona', 'Phoenix', '005 17', 'Fallview', 511, null, null, 42);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (57, to_date('16-Apr-20', 'DD-MON-RR'), null, '1', 409.35, 18, 92, '0', 'justo.txt', 'Maryland', 'Baltimore',
        '273 55', null, 804, 75, 'Maryland', 'Baltimore', '273 55', null, 804, 75, null, 8);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (58, to_date('17-Apr-20', 'DD-MON-RR'), null, '1', 829.75, 18, 104, '0', 'mi.txt', 'Kentucky', 'Louisville',
        '350 88', 'Katie', 218, 51, 'Kentucky', 'Louisville', '350 88', 'Katie', 218, 51, null, 70);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (59, to_date('16-Apr-20', 'DD-MON-RR'), null, '0', 305.06, 18, 100, '0', 'montes_nascetur_ridiculus.txt',
        'California', 'Berkeley', '932 63', 'Namekagon', 993, null, 'Illinois', 'Springfield', '236 35', null, 586,
        33, null, 94);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (60, to_date('03-Mar-20', 'DD-MON-RR'), to_date('03-Apr-20', 'DD-MON-RR'), '0', 786.59, 16, 90, '0',
        'sit_amet_sapien.txt', 'Nebraska', 'Omaha', '622 96', null, 839, 41, 'California', 'Berkeley', '932 63',
        'Namekagon', 993, null, null, 46);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (61, to_date('29-Mar-20', 'DD-MON-RR'), to_date('29-Apr-20', 'DD-MON-RR'), '0', 222.19, 18, 97, '0',
        'in.txt', 'Illinois', 'Springfield', '236 35', null, 586, 33, 'Nebraska', 'Omaha', '622 96', null, 839, 41,
        'id lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi eu orci mauris', 58);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (62, to_date('05-Apr-20', 'DD-MON-RR'), null, '0', 995.56, 15, 80, '1', 'quam.txt', 'Mississippi',
        'Meridian', '269 78', null, 935, null, 'Mississippi', 'Meridian', '269 78', null, 935, null, null, 39);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (63, to_date('01-Mar-20', 'DD-MON-RR'), to_date('01-Apr-20', 'DD-MON-RR'), '1', 289.45, 13, 86, '0',
        'et_ultrices.txt', 'Texas', 'Houston', '914 13', null, 139, 66, 'Texas', 'Houston', '914 13', null, 139, 66,
        null, 86);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (64, to_date('23-Feb-20', 'DD-MON-RR'), to_date('23-Mar-20', 'DD-MON-RR'), '1', 871.38, 19, 83, '0',
        'amet.txt', 'North Carolina', 'High Point', '097 89', 'Gerald', 805, 17, 'North Carolina', 'High Point',
        '097 89', 'Gerald', 805, 17, null, 21);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (65, to_date('24-Apr-20', 'DD-MON-RR'), null, '0', 329.15, 14, 79, '1', 'sapien_iaculis_congue.txt',
        'Michigan', 'Southfield', '566 02', 'Rutledge', 862, 33, 'Michigan', 'Southfield', '566 02', 'Rutledge',
        862, 33, null, 45);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (66, to_date('29-Apr-20', 'DD-MON-RR'), null, '0', 812.88, 16, 108, '0', 'magnis.txt', 'California',
        'Santa Monica', '547 99', 'Arkansas', 508, 18, 'California', 'Santa Monica', '547 99', 'Arkansas', 508, 18,
        null, 4);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (67, to_date('25-Jan-20', 'DD-MON-RR'), to_date('25-Feb-20', 'DD-MON-RR'), '0', 767.09, 10, 117, '0',
        'a_suscipit_nulla.txt', 'Texas', 'Denton', '349 10', 'Talmadge', 431, 18, 'Texas', 'Denton', '349 10',
        'Talmadge', 431, 18, null, 27);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (68, to_date('15-Feb-20', 'DD-MON-RR'), to_date('15-Mar-20', 'DD-MON-RR'), '0', 540.68, 20, 112, '1',
        'pretium.txt', 'Pennsylvania', 'Harrisburg', '190 25', 'Upham', 332, 68, 'Pennsylvania', 'Harrisburg',
        '190 25', 'Upham', 332, 68, null, 9);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (69, to_date('20-Jan-20', 'DD-MON-RR'), to_date('20-Feb-20', 'DD-MON-RR'), '1', 743.14, 20, 104, '1',
        'rutrum_at_lorem.txt', 'California', 'Santa Monica', '198 97', null, 181, null, 'California',
        'Santa Monica', '198 97', null, 181, null, null, 90);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (70, to_date('22-Jan-20', 'DD-MON-RR'), to_date('22-Feb-20', 'DD-MON-RR'), '1', 285.64, 17, 61, '1',
        'justo_in_hac.txt', 'Massachusetts', 'Springfield', '020 38', 'Maryland', 759, 75, 'Massachusetts',
        'Springfield', '020 38', 'Maryland', 759, 75, null, 12);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (71, to_date('12-Feb-20', 'DD-MON-RR'), to_date('12-Mar-20', 'DD-MON-RR'), '1', 321.64, 15, 77, '0',
        'at_nunc.txt', 'New York', 'Yonkers', '058 72', null, 314, 82, 'New York', 'Yonkers', '058 72', null, 314,
        82, null, 5);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (72, to_date('03-Jan-20', 'DD-MON-RR'), to_date('03-Feb-20', 'DD-MON-RR'), '1', 337.24, 18, 72, '1',
        'diam_nam.txt', 'Massachusetts', 'Boston', '330 61', 'Warbler', 213, 33, 'Massachusetts', 'Boston',
        '330 61', 'Warbler', 213, 33, null, 59);

insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (74, to_date('03-Mar-20', 'DD-MON-RR'), to_date('03-Apr-20', 'DD-MON-RR'), '0', 924.47, 13, 94, '0',
        'duis_ac_nibh.txt', 'Minnesota', 'Saint Paul', '021 60', 'Logan', 205, 31, 'Minnesota', 'Saint Paul',
        '021 60', 'Logan', 205, 31, null, 77);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (75, to_date('13-Jan-20', 'DD-MON-RR'), to_date('13-Feb-20', 'DD-MON-RR'), '0', 842.52, 12, 119, '1',
        'varius_integer_ac.txt', 'California', 'Sacramento', '832 06', 'Lawn', 148, 75, 'California', 'Sacramento',
        '832 06', 'Lawn', 148, 75, null, 82);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (76, to_date('20-Jan-20', 'DD-MON-RR'), to_date('20-Feb-20', 'DD-MON-RR'), '0', 680.36, 19, 80, '0',
        'morbi.txt', 'District of Columbia', 'Washington', '627 33', 'Dwight', 216, 56, 'District of Columbia',
        'Washington', '627 33', 'Dwight', 216, 56, null, 18);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (77, to_date('19-Jan-20', 'DD-MON-RR'), to_date('19-Feb-20', 'DD-MON-RR'), '0', 404.38, 13, 116, '0',
        'est_risus_auctor.txt', 'Alabama', 'Huntsville', '527 31', 'Lillian', 619, 24, 'Alabama', 'Huntsville',
        '527 31', 'Lillian', 619, 24, null, 65);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (78, to_date('23-Mar-20', 'DD-MON-RR'), to_date('23-Apr-20', 'DD-MON-RR'), '1', 665.61, 18, 58, '0',
        'nulla_ac_enim.txt', 'West Virginia', 'Huntington', '769 54', 'Banding', 237, null, 'West Virginia',
        'Huntington', '769 54', 'Banding', 237, null, null, 92);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (79, to_date('24-Feb-20', 'DD-MON-RR'), to_date('24-Mar-20', 'DD-MON-RR'), '1', 292.78, 15, 79, '1',
        'turpis_enim_blandit.txt', 'New York', 'New York City', '468 97', 'Cambridge', 251, 32, 'New York',
        'New York City', '468 97', 'Cambridge', 251, 32, null, 72);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (80, to_date('26-Jan-20', 'DD-MON-RR'), to_date('26-Feb-20', 'DD-MON-RR'), '0', 629.01, 13, 62, '0',
        'commodo.txt', 'Alabama', 'Mobile', '686 97', 'Troy', 339, null, 'Alabama', 'Mobile', '686 97', 'Troy', 339,
        null, null, 88);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (81, to_date('14-Feb-20', 'DD-MON-RR'), to_date('14-Mar-20', 'DD-MON-RR'), '1', 945.1, 16, 112, '1',
        'consectetuer_adipiscing_elit.txt', 'Texas', 'Amarillo', '028 78', null, 295, 24, 'Texas', 'Amarillo',
        '028 78', null, 295, 24,
        'ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus et',
        60);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (82, to_date('14-Mar-20', 'DD-MON-RR'), to_date('14-Apr-20', 'DD-MON-RR'), '1', 673.98, 13, 96, '1',
        'pede_justo_lacinia.txt', 'Illinois', 'Chicago', '072 10', 'Superior', 687, null, 'Illinois', 'Chicago',
        '072 10', 'Superior', 687, null, null, 21);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (83, to_date('03-Feb-20', 'DD-MON-RR'), to_date('03-Mar-20', 'DD-MON-RR'), '0', 446.25, 12, 94, '1',
        'odio_cras.txt', 'Florida', 'Punta Gorda', '400 07', null, 230, null, 'Florida', 'Tallahassee', '754 20',
        'Del Sol', 405, 14, null, 5);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (84, to_date('01-Jan-20', 'DD-MON-RR'), to_date('01-Feb-20', 'DD-MON-RR'), '0', 689.73, 17, 51, '0',
        'cursus_vestibulum_proin.txt', 'Texas', 'Dallas', '908 67', 'Fallview', 235, 57, 'Florida', 'Punta Gorda',
        '400 07', null, 230, null, null, 35);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (85, to_date('26-Feb-20', 'DD-MON-RR'), to_date('26-Mar-20', 'DD-MON-RR'), '1', 944.98, 14, 56, '0',
        'eu.txt', 'Florida', 'Jacksonville', '852 48', 'Schmedeman', 284, 34, 'Texas', 'Dallas', '908 67',
        'Fallview', 235, 57, null, 71);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (86, to_date('17-Apr-20', 'DD-MON-RR'), null, '1', 705.86, 20, 85, '0', 'vulputate_elementum_nullam.txt',
        'Oklahoma', 'Oklahoma City', '565 90', 'Pleasure', 755, null, 'Pennsylvania', 'Harrisburg', '905 48',
        'Hauk', 914, 8, null, 20);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (87, to_date('09-Jan-20', 'DD-MON-RR'), to_date('09-Feb-20', 'DD-MON-RR'), '1', 617.33, 13, 73, '1',
        'magnis_dis.txt', 'Pennsylvania', 'Harrisburg', '905 48', 'Hauk', 914, 8, 'Oklahoma', 'Oklahoma City',
        '565 90', 'Pleasure', 755, null, null, 71);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (88, to_date('03-Jan-20', 'DD-MON-RR'), to_date('03-Feb-20', 'DD-MON-RR'), '1', 854.15, 12, 116, '0',
        'pretium_iaculis.txt', 'Missouri', 'Lees Summit', '428 26', 'Bashford', 500, 30, 'Missouri', 'Lees Summit',
        '428 26', 'Bashford', 500, 30, null, 71);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (89, to_date('06-Mar-20', 'DD-MON-RR'), to_date('06-Apr-20', 'DD-MON-RR'), '0', 794.21, 14, 72, '0',
        'quam_a.txt', 'Florida', 'Tallahassee', '754 20', 'Del Sol', 405, 14, 'Kansas', 'Topeka', '858 88',
        'Drewry', 709, 30, null, 23);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (90, to_date('23-Mar-20', 'DD-MON-RR'), to_date('23-Apr-20', 'DD-MON-RR'), '0', 486.68, 16, 117, '0',
        'nulla_ultrices_aliquet.txt', 'Alabama', 'Birmingham', '042 77', 'Helena', 777, 34, 'Alabama', 'Birmingham',
        '042 77', 'Helena', 777, 34, null, 56);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (91, to_date('08-Jan-20', 'DD-MON-RR'), to_date('08-Feb-20', 'DD-MON-RR'), '1', 877.09, 16, 105, '0',
        'aliquet_ultrices_erat.txt', 'Kansas', 'Topeka', '858 88', 'Drewry', 709, 30, 'New York', 'Albany',
        '945 13', null, 265, 45, null, 47);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (92, to_date('14-Apr-20', 'DD-MON-RR'), null, '1', 401.61, 14, 71, '0', 'rutrum.txt', 'New York', 'Albany',
        '945 13', null, 265, 45, 'California', 'Van Nuys', '028 48', null, 805, 36, null, 31);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (93, to_date('16-Mar-20', 'DD-MON-RR'), to_date('16-Apr-20', 'DD-MON-RR'), '1', 587.95, 10, 88, '1',
        'pellentesque_ultrices.txt', 'California', 'Van Nuys', '028 48', null, 805, 36, 'Florida', 'Jacksonville',
        '852 48', 'Schmedeman', 284, 34,
        'lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget',
        17);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (94, to_date('17-Apr-20', 'DD-MON-RR'), null, '1', 845.6, 10, 95, '0', 'diam.txt', 'Utah', 'Salt Lake City',
        '957 74', 'Old Gate', 790, 28, 'Utah', 'Salt Lake City', '957 74', 'Old Gate', 790, 28, null, 72);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (95, to_date('24-Apr-20', 'DD-MON-RR'), null, '1', 321.21, 16, 72, '1', 'in_consequat.txt', 'Arizona',
        'Tucson', '179 57', 'Burrows', 272, null, 'Arizona', 'Tucson', '179 57', 'Burrows', 272, null, null, 40);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (96, to_date('23-Apr-20', 'DD-MON-RR'), null, '0', 285.79, 14, 59, '1', 'penatibus_et.txt', 'Iowa',
        'Des Moines', '392 86', 'Westend', 925, null, 'Massachusetts', 'Newton', '530 09', 'Summerview', 567, 6,
        null, 84);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (97, to_date('11-Apr-20', 'DD-MON-RR'), null, '0', 651.03, 18, 68, '0', 'ac.txt', 'Missouri', 'Kansas City',
        '508 15', 'Elmside', 575, 58, 'Iowa', 'Des Moines', '392 86', 'Westend', 925, null, null, 15);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (98, to_date('25-Feb-20', 'DD-MON-RR'), to_date('25-Mar-20', 'DD-MON-RR'), '0', 855.2, 16, 106, '0',
        'accumsan.txt', 'Connecticut', 'Hartford', '021 89', 'Arrowood', 250, null, 'Missouri', 'Kansas City',
        '508 15', 'Elmside', 575, 58, null, 13);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (99, to_date('10-Mar-20', 'DD-MON-RR'), to_date('10-Apr-20', 'DD-MON-RR'), '1', 460.7, 15, 86, '1',
        'nisi_at.txt', 'South Carolina', 'Myrtle Beach', '816 90', 'Scofield', 306, 71, 'Connecticut', 'Hartford',
        '021 89', 'Arrowood', 250, null, null, 53);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (100, to_date('08-Mar-20', 'DD-MON-RR'), to_date('08-Apr-20', 'DD-MON-RR'), '1', 529.76, 11, 88, '1',
        'lorem_id_ligula.txt', 'Florida', 'Miami', '883 20', null, 705, 50, 'South Carolina', 'Myrtle Beach',
        '816 90', 'Scofield', 306, 71, null, 65);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (101, to_date('05-Feb-20', 'DD-MON-RR'), to_date('05-Mar-20', 'DD-MON-RR'), '0', 825.3, 12, 98, '1',
        'odio.txt', 'Oregon', 'Salem', '611 87', null, 748, 64, 'Florida', 'Miami', '883 20', null, 705, 50, null,
        45);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (102, to_date('14-Jan-20', 'DD-MON-RR'), to_date('14-Feb-20', 'DD-MON-RR'), '1', 406.22, 10, 66, '0',
        'feugiat.txt', 'Massachusetts', 'Newton', '530 09', 'Summerview', 567, 6, 'Oregon', 'Salem', '611 87', null,
        748, 64, null, 42);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (103, to_date('20-Jan-20', 'DD-MON-RR'), to_date('20-Feb-20', 'DD-MON-RR'), '0', 717.24, 13, 63, '0',
        'euismod_scelerisque.txt', 'Florida', 'Pensacola', '355 91', 'Fairview', 589, 75, 'Florida', 'Pensacola',
        '355 91', 'Fairview', 589, 75,
        'gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi', 59);

insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (105, to_date('06-Feb-20', 'DD-MON-RR'), to_date('06-Mar-20', 'DD-MON-RR'), '0', 402.43, 14, 116, '1',
        'id_nulla.txt', 'California', 'Los Angeles', '066 44', 'Brickson Park', 999, 3, 'California', 'Los Angeles',
        '066 44', 'Brickson Park', 999, 3,
        'vel nisl duis ac nibh fusce lacus purus aliquet at feugiat non pretium quis lectus suspendisse potenti in eleifend quam',
        34);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (106, to_date('08-Apr-20', 'DD-MON-RR'), null, '1', 205.58, 19, 108, '0', 'nullam.txt', 'Colorado', 'Denver',
        '531 13', null, 722, 96, 'Georgia', 'Atlanta', '139 87', 'Northfield', 934, 95, null, 31);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (107, to_date('27-Jan-20', 'DD-MON-RR'), to_date('27-Feb-20', 'DD-MON-RR'), '1', 786.02, 19, 93, '1',
        'proin_leo_odio.txt', 'Virginia', 'Lynchburg', '099 90', 'Esch', 758, 88, 'Colorado', 'Denver', '531 13',
        null, 722, 96, null, 53);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (108, to_date('12-Feb-20', 'DD-MON-RR'), to_date('12-Mar-20', 'DD-MON-RR'), '0', 937.23, 11, 51, '0',
        'nibh_ligula.txt', 'District of Columbia', 'Washington', '273 54', 'Sloan', 865, 32, 'Virginia',
        'Lynchburg', '099 90', 'Esch', 758, 88, null, 9);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (109, to_date('23-Mar-20', 'DD-MON-RR'), to_date('23-Apr-20', 'DD-MON-RR'), '1', 398.03, 20, 104, '1',
        'sit_amet_cursus.txt', 'Florida', 'Naples', '644 24', null, 231, 27, 'District of Columbia', 'Washington',
        '273 54', 'Sloan', 865, 32, null, 44);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (110, to_date('24-Mar-20', 'DD-MON-RR'), to_date('24-Apr-20', 'DD-MON-RR'), '1', 627.88, 18, 53, '1',
        'faucibus.txt', 'Florida', 'Lehigh Acres', '907 01', 'Florence', 647, 35, 'Florida', 'Naples', '644 24',
        null, 231, 27, null, 4);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (111, to_date('18-Apr-20', 'DD-MON-RR'), null, '1', 603.22, 13, 76, '1', 'cras.txt', 'California', 'Orange',
        '257 04', 'Packers', 691, 29, 'Florida', 'Lehigh Acres', '907 01', 'Florence', 647, 35,
        'dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus', 7);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (112, to_date('25-Mar-20', 'DD-MON-RR'), to_date('25-Apr-20', 'DD-MON-RR'), '0', 674.08, 16, 72, '0',
        'velit_nec.txt', 'Georgia', 'Atlanta', '139 87', 'Northfield', 934, 95, 'California', 'Orange', '257 04',
        'Packers', 691, 29, null, 42);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (113, to_date('14-Apr-20', 'DD-MON-RR'), null, '1', 650.72, 19, 51, '0', 'donec_semper.txt', 'Texas',
        'Austin', '659 13', 'Comanche', 638, 78, 'Texas', 'Austin', '659 13', 'Comanche', 638, 78, null, 73);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (114, to_date('04-Mar-20', 'DD-MON-RR'), to_date('04-Apr-20', 'DD-MON-RR'), '1', 365.64, 10, 76, '0',
        'lectus_aliquam.txt', 'Arizona', 'Phoenix', '005 17', 'Fallview', 511, null, 'Arizona', 'Phoenix', '005 17',
        'Fallview', 511, null, null, 28);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (115, to_date('19-Feb-20', 'DD-MON-RR'), to_date('19-Mar-20', 'DD-MON-RR'), '0', 502.68, 13, 114, '0',
        'mi_pede.txt', 'California', 'Sacramento', '257 69', 'West', 843, null, 'California', 'Sacramento',
        '257 69', 'West', 843, null, null, 51);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (116, to_date('21-Feb-20', 'DD-MON-RR'), to_date('21-Mar-20', 'DD-MON-RR'), '0', 881.31, 12, 106, '1',
        'donec.txt', 'Michigan', 'Detroit', '651 81', null, 714, 57, 'Michigan', 'Detroit', '651 81', null, 714, 57,
        null, 13);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (117, to_date('21-Feb-20', 'DD-MON-RR'), to_date('21-Mar-20', 'DD-MON-RR'), '1', 627.8, 10, 111, '1',
        'magna_at.txt', 'Iowa', 'Des Moines', '577 33', 'Tennyson', 166, null, 'Iowa', 'Des Moines', '577 33',
        'Tennyson', 166, null, null, 59);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (118, to_date('23-Jan-20', 'DD-MON-RR'), to_date('23-Feb-20', 'DD-MON-RR'), '1', 246.7, 10, 91, '0',
        'vehicula_consequat.txt', 'Mississippi', 'Hattiesburg', '421 91', 'Chinook', 531, 35, 'Alaska', 'Anchorage',
        '038 93', 'Reindahl', 188, null, null, 58);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (119, to_date('04-Feb-20', 'DD-MON-RR'), to_date('04-Mar-20', 'DD-MON-RR'), '1', 908.73, 12, 71, '1',
        'mollis_molestie.txt', 'West Virginia', 'Huntington', '953 63', null, 169, 48, 'Mississippi', 'Hattiesburg',
        '421 91', 'Chinook', 531, 35, null, 63);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (120, to_date('20-Mar-20', 'DD-MON-RR'), to_date('20-Apr-20', 'DD-MON-RR'), '1', 233.97, 16, 99, '0',
        'sapien.txt', 'Alabama', 'Birmingham', '691 85', 'Northwestern', 565, 41, 'West Virginia', 'Huntington',
        '953 63', null, 169, 48, null, 77);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (121, to_date('28-Mar-20', 'DD-MON-RR'), to_date('28-Apr-20', 'DD-MON-RR'), '0', 736.76, 16, 62, '1',
        'non_mi_integer.txt', 'Virginia', 'Herndon', '590 25', 'Magdeline', 839, 19, 'Alabama', 'Birmingham',
        '691 85', 'Northwestern', 565, 41, null, 11);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (122, to_date('27-Jan-20', 'DD-MON-RR'), to_date('27-Feb-20', 'DD-MON-RR'), '1', 968.89, 14, 60, '0',
        'laoreet_ut_rhoncus.txt', 'Louisiana', 'Baton Rouge', '238 74', 'Sage', 351, null, 'Virginia', 'Herndon',
        '590 25', 'Magdeline', 839, 19, null, 90);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (123, to_date('19-Apr-20', 'DD-MON-RR'), null, '1', 958.88, 20, 104, '1', 'placerat.txt', 'Utah',
        'Salt Lake City', '844 99', null, 603, 79, 'Louisiana', 'Baton Rouge', '238 74', 'Sage', 351, null, null,
        58);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (124, to_date('29-Mar-20', 'DD-MON-RR'), to_date('29-Apr-20', 'DD-MON-RR'), '1', 307.59, 16, 64, '1',
        'dapibus.txt', 'Alaska', 'Anchorage', '038 93', 'Reindahl', 188, null, 'Utah', 'Salt Lake City', '844 99',
        null, 603, 79, null, 59);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (125, to_date('20-Apr-20', 'DD-MON-RR'), null, '1', 250.21, 14, 115, '1', 'aliquam.txt', 'California',
        'San Diego', '424 85', 'Vera', 510, null, 'California', 'San Diego', '424 85', 'Vera', 510, null, null, 26);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (126, to_date('11-Apr-20', 'DD-MON-RR'), null, '1', 706.93, 18, 82, '0', 'sapien.txt', 'Florida', 'Miami',
        '329 86', null, 707, null, 'Florida', 'Miami', '329 86', null, 707, null, null, 63);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (127, to_date('04-Jan-20', 'DD-MON-RR'), to_date('04-Feb-20', 'DD-MON-RR'), '0', 441.33, 11, 101, '0',
        'vulputate.txt', 'California', 'Oceanside', '601 60', null, 515, 76, 'California', 'Oceanside', '601 60',
        null, 515, 76, null, 20);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (128, to_date('01-Feb-20', 'DD-MON-RR'), to_date('01-Mar-20', 'DD-MON-RR'), '0', 202.08, 17, 65, '0',
        'cubilia_curae_nulla.txt', 'Maryland', 'Baltimore', '273 55', null, 804, 75, 'Maryland', 'Baltimore',
        '273 55', null, 804, 75, null, 4);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (129, to_date('02-Mar-20', 'DD-MON-RR'), to_date('02-Apr-20', 'DD-MON-RR'), '1', 262.84, 14, 96, '1',
        'sit.txt', 'Kentucky', 'Louisville', '350 88', 'Katie', 218, 51, 'Kentucky', 'Louisville', '350 88',
        'Katie', 218, 51, null, 45);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (130, to_date('13-Mar-20', 'DD-MON-RR'), to_date('13-Apr-20', 'DD-MON-RR'), '0', 579.66, 13, 97, '1',
        'facilisi.txt', 'California', 'Berkeley', '932 63', 'Namekagon', 993, null, 'California', 'Berkeley',
        '932 63', 'Namekagon', 993, null, null, 14);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (131, to_date('07-Apr-20', 'DD-MON-RR'), null, '1', 746.39, 13, 111, '1', 'semper_rutrum_nulla.txt',
        'Nebraska', 'Omaha', '622 96', null, 839, 41, 'Texas', 'Denton', '349 10', 'Talmadge', 431, 18,
        'vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit', 32);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (132, to_date('17-Feb-20', 'DD-MON-RR'), to_date('17-Mar-20', 'DD-MON-RR'), '1', 276.48, 16, 51, '1',
        'convallis.txt', 'Illinois', 'Springfield', '236 35', null, 586, 33, 'Nebraska', 'Omaha', '622 96', null,
        839, 41, null, 40);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (133, to_date('15-Apr-20', 'DD-MON-RR'), null, '0', 248.98, 13, 74, '0', 'nulla.txt', 'Mississippi',
        'Meridian', '269 78', null, 935, null, 'Illinois', 'Springfield', '236 35', null, 586, 33, null, 54);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (134, to_date('17-Feb-20', 'DD-MON-RR'), to_date('17-Mar-20', 'DD-MON-RR'), '1', 417.99, 16, 61, '0',
        'sed_magna_at.txt', 'Texas', 'Houston', '914 13', null, 139, 66, 'Mississippi', 'Meridian', '269 78', null,
        935, null, 'at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis', 44);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (135, to_date('09-Jan-20', 'DD-MON-RR'), to_date('09-Feb-20', 'DD-MON-RR'), '1', 294.97, 17, 63, '0',
        'felis_sed_lacus.txt', 'North Carolina', 'High Point', '097 89', 'Gerald', 805, 17, 'Texas', 'Houston',
        '914 13', null, 139, 66, null, 75);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (136, to_date('08-Apr-20', 'DD-MON-RR'), null, '0', 525.55, 12, 56, '0', 'id_ornare.txt', 'Michigan',
        'Southfield', '566 02', 'Rutledge', 862, 33, 'North Carolina', 'High Point', '097 89', 'Gerald', 805, 17,
        'sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem', 23);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (137, to_date('10-Apr-20', 'DD-MON-RR'), null, '1', 868.45, 18, 62, '0', 'volutpat_erat.txt', 'California',
        'Santa Monica', '547 99', 'Arkansas', 508, 18, 'Michigan', 'Southfield', '566 02', 'Rutledge', 862, 33,
        null, 73);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (138, to_date('14-Apr-20', 'DD-MON-RR'), null, '0', 372.18, 13, 70, '1', 'nulla_ut_erat.txt', 'Texas',
        'Denton', '349 10', 'Talmadge', 431, 18, 'California', 'Santa Monica', '547 99', 'Arkansas', 508, 18, null,
        85);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (139, to_date('27-Apr-20', 'DD-MON-RR'), null, '1', 210.74, 20, 78, '1', 'justo.txt', 'Pennsylvania',
        'Harrisburg', '190 25', 'Upham', 332, 68, 'Pennsylvania', 'Harrisburg', '190 25', 'Upham', 332, 68,
        'ligula suspendisse ornare consequat lectus in est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla',
        72);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (140, to_date('18-Jan-20', 'DD-MON-RR'), to_date('18-Feb-20', 'DD-MON-RR'), '1', 587.63, 13, 65, '1',
        'et.txt', 'California', 'Santa Monica', '198 97', null, 181, null, 'California', 'Santa Monica', '198 97',
        null, 181, null, null, 86);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (141, to_date('05-Apr-20', 'DD-MON-RR'), null, '1', 368.55, 12, 82, '0', 'vestibulum_ante.txt',
        'Massachusetts', 'Springfield', '020 38', 'Maryland', 759, 75, 'Massachusetts', 'Springfield', '020 38',
        'Maryland', 759, 75, null, 84);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (142, to_date('12-Feb-20', 'DD-MON-RR'), to_date('12-Mar-20', 'DD-MON-RR'), '0', 935.38, 18, 113, '0',
        'velit_eu.txt', 'New York', 'Yonkers', '058 72', null, 314, 82, 'New York', 'Yonkers', '058 72', null, 314,
        82, null, 66);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (143, to_date('26-Feb-20', 'DD-MON-RR'), to_date('26-Mar-20', 'DD-MON-RR'), '1', 432.7, 13, 77, '1',
        'nulla_suscipit.txt', 'Massachusetts', 'Boston', '330 61', 'Warbler', 213, 33, 'Massachusetts', 'Boston',
        '330 61', 'Warbler', 213, 33,
        'accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec',
        65);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (144, to_date('20-Mar-20', 'DD-MON-RR'), to_date('20-Apr-20', 'DD-MON-RR'), '0', 318.22, 13, 81, '1',
        'sem_mauris_laoreet.txt', 'Oklahoma', 'Edmond', '491 40', null, 130, null, 'Oklahoma', 'Edmond', '491 40',
        null, 130, null, null, 34);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (145, to_date('29-Mar-20', 'DD-MON-RR'), to_date('29-Apr-20', 'DD-MON-RR'), '1', 469.7, 10, 85, '1',
        'ultrices_posuere.txt', 'Minnesota', 'Saint Paul', '021 60', 'Logan', 205, 31, 'Minnesota', 'Saint Paul',
        '021 60', 'Logan', 205, 31, null, 29);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (146, to_date('23-Jan-20', 'DD-MON-RR'), to_date('23-Feb-20', 'DD-MON-RR'), '1', 345.47, 14, 76, '0',
        'suspendisse.txt', 'California', 'Sacramento', '832 06', 'Lawn', 148, 75, 'California', 'Sacramento',
        '832 06', 'Lawn', 148, 75, null, 32);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (147, to_date('04-Jan-20', 'DD-MON-RR'), to_date('04-Feb-20', 'DD-MON-RR'), '0', 548.64, 17, 115, '1',
        'tincidunt.txt', 'District of Columbia', 'Washington', '627 33', 'Dwight', 216, 56, 'District of Columbia',
        'Washington', '627 33', 'Dwight', 216, 56, null, 46);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (148, to_date('12-Jan-20', 'DD-MON-RR'), to_date('12-Feb-20', 'DD-MON-RR'), '0', 865.14, 20, 62, '1',
        'sed_tristique.txt', 'Alabama', 'Huntsville', '527 31', 'Lillian', 619, 24, 'Alabama', 'Huntsville',
        '527 31', 'Lillian', 619, 24,
        'lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros',
        25);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (149, to_date('03-Feb-20', 'DD-MON-RR'), to_date('03-Mar-20', 'DD-MON-RR'), '1', 292.69, 18, 103, '0',
        'sit_amet.txt', 'West Virginia', 'Huntington', '769 54', 'Banding', 237, null, 'West Virginia',
        'Huntington', '769 54', 'Banding', 237, null, null, 85);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (150, to_date('10-Mar-20', 'DD-MON-RR'), to_date('10-Apr-20', 'DD-MON-RR'), '0', 947.09, 15, 98, '0',
        'lobortis_est.txt', 'New York', 'New York City', '468 97', 'Cambridge', 251, 32, 'New York',
        'New York City', '468 97', 'Cambridge', 251, 32, null, 37);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (151, to_date('17-Jan-20', 'DD-MON-RR'), to_date('17-Feb-20', 'DD-MON-RR'), '0', 495.45, 20, 83, '1',
        'tellus.txt', 'Alabama', 'Mobile', '686 97', 'Troy', 339, null, 'Alabama', 'Mobile', '686 97', 'Troy', 339,
        null, null, 59);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (152, to_date('19-Apr-20', 'DD-MON-RR'), null, '0', 267.97, 20, 86, '1', 'iaculis_justo.txt', 'Texas',
        'Amarillo', '028 78', null, 295, 24, 'Texas', 'Amarillo', '028 78', null, 295, 24, null, 63);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (153, to_date('09-Feb-20', 'DD-MON-RR'), to_date('09-Mar-20', 'DD-MON-RR'), '1', 751.63, 11, 66, '0',
        'interdum.txt', 'Illinois', 'Chicago', '072 10', 'Superior', 687, null, 'Illinois', 'Chicago', '072 10',
        'Superior', 687, null,
        'nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris', 44);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (154, to_date('12-Apr-20', 'DD-MON-RR'), null, '0', 672.61, 12, 109, '1', 'sit.txt', 'Florida',
        'Punta Gorda', '400 07', null, 230, null, 'Florida', 'Punta Gorda', '400 07', null, 230, null, null, 43);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (155, to_date('08-Jan-20', 'DD-MON-RR'), to_date('08-Feb-20', 'DD-MON-RR'), '0', 243.76, 11, 118, '0',
        'commodo_placerat_praesent.txt', 'Texas', 'Dallas', '908 67', 'Fallview', 235, 57, 'Texas', 'Dallas',
        '908 67', 'Fallview', 235, 57,
        'penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum', 93);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (156, to_date('03-Apr-20', 'DD-MON-RR'), null, '1', 887.17, 12, 80, '0', 'libero.txt', 'Florida',
        'Jacksonville', '852 48', 'Schmedeman', 284, 34, 'Florida', 'Jacksonville', '852 48', 'Schmedeman', 284, 34,
        null, 9);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (157, to_date('28-Feb-20', 'DD-MON-RR'), to_date('28-Mar-20', 'DD-MON-RR'), '1', 956.8, 13, 90, '1',
        'accumsan_tellus.txt', 'Oklahoma', 'Oklahoma City', '565 90', 'Pleasure', 755, null, 'Oklahoma',
        'Oklahoma City', '565 90', 'Pleasure', 755, null, null, 79);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (158, to_date('03-Mar-20', 'DD-MON-RR'), to_date('03-Apr-20', 'DD-MON-RR'), '1', 616.38, 19, 115, '1',
        'in.txt', 'Pennsylvania', 'Harrisburg', '905 48', 'Hauk', 914, 8, 'Pennsylvania', 'Harrisburg', '905 48',
        'Hauk', 914, 8,
        'nibh in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum',
        72);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (159, to_date('26-Jan-20', 'DD-MON-RR'), to_date('26-Feb-20', 'DD-MON-RR'), '0', 331.09, 16, 99, '0',
        'vel_augue.txt', 'Missouri', 'Lees Summit', '428 26', 'Bashford', 500, 30, 'Missouri', 'Lees Summit',
        '428 26', 'Bashford', 500, 30, null, 14);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (160, to_date('22-Jan-20', 'DD-MON-RR'), to_date('22-Feb-20', 'DD-MON-RR'), '1', 485.29, 19, 56, '0',
        'sociis_natoque_penatibus.txt', 'Florida', 'Tallahassee', '754 20', 'Del Sol', 405, 14, 'Florida',
        'Tallahassee', '754 20', 'Del Sol', 405, 14, null, 84);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (161, to_date('29-Apr-20', 'DD-MON-RR'), null, '1', 728.34, 13, 109, '0', 'nibh_ligula_nec.txt', 'Alabama',
        'Birmingham', '042 77', 'Helena', 777, 34, 'Alabama', 'Birmingham', '042 77', 'Helena', 777, 34, null, 58);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (162, to_date('18-Apr-20', 'DD-MON-RR'), null, '1', 947.34, 20, 82, '0', 'amet_nulla_quisque.txt', 'Kansas',
        'Topeka', '858 88', 'Drewry', 709, 30, 'Kansas', 'Topeka', '858 88', 'Drewry', 709, 30, null, 61);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (163, to_date('01-Mar-20', 'DD-MON-RR'), to_date('01-Apr-20', 'DD-MON-RR'), '1', 912.18, 18, 99, '0',
        'elementum.txt', 'New York', 'Albany', '945 13', null, 265, 45, 'New York', 'Albany', '945 13', null, 265,
        45, null, 32);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (164, to_date('06-Jan-20', 'DD-MON-RR'), to_date('06-Feb-20', 'DD-MON-RR'), '0', 889.75, 19, 87, '0',
        'enim.txt', 'California', 'Van Nuys', '028 48', null, 805, 36, 'California', 'Van Nuys', '028 48', null,
        805, 36, null, 71);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (165, to_date('04-Jan-20', 'DD-MON-RR'), to_date('04-Feb-20', 'DD-MON-RR'), '1', 307.82, 19, 76, '0',
        'quisque_porta.txt', 'Utah', 'Salt Lake City', '957 74', 'Old Gate', 790, 28, 'Utah', 'Salt Lake City',
        '957 74', 'Old Gate', 790, 28,
        'semper interdum mauris ullamcorper purus sit amet nulla quisque arcu libero rutrum ac lobortis vel dapibus at',
        87);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (166, to_date('11-Mar-20', 'DD-MON-RR'), to_date('11-Apr-20', 'DD-MON-RR'), '1', 265.75, 16, 110, '0',
        'sem.txt', 'Arizona', 'Tucson', '179 57', 'Burrows', 272, null, 'Arizona', 'Tucson', '179 57', 'Burrows',
        272, null, null, 68);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (167, to_date('02-Apr-20', 'DD-MON-RR'), null, '1', 618.23, 17, 76, '1', 'ultrices.txt', 'Iowa',
        'Des Moines', '392 86', 'Westend', 925, null, 'Iowa', 'Des Moines', '392 86', 'Westend', 925, null, null,
        26);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (168, to_date('01-Apr-20', 'DD-MON-RR'), null, '0', 504.89, 14, 54, '0', 'aliquet_massa_id.txt', 'Missouri',
        'Kansas City', '508 15', 'Elmside', 575, 58, 'Missouri', 'Kansas City', '508 15', 'Elmside', 575, 58,
        'metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris',
        90);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (169, to_date('13-Mar-20', 'DD-MON-RR'), to_date('13-Apr-20', 'DD-MON-RR'), '0', 944.74, 19, 106, '1',
        'pulvinar_nulla_pede.txt', 'Connecticut', 'Hartford', '021 89', 'Arrowood', 250, null, 'Connecticut',
        'Hartford', '021 89', 'Arrowood', 250, null, null, 92);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (170, to_date('11-Mar-20', 'DD-MON-RR'), to_date('11-Apr-20', 'DD-MON-RR'), '0', 222.98, 17, 95, '0',
        'volutpat_quam.txt', 'South Carolina', 'Myrtle Beach', '816 90', 'Scofield', 306, 71, 'Colorado', 'Denver',
        '531 13', null, 722, 96, null, 12);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (171, to_date('27-Feb-20', 'DD-MON-RR'), to_date('27-Mar-20', 'DD-MON-RR'), '1', 681.16, 16, 103, '0',
        'morbi_vestibulum.txt', 'Florida', 'Miami', '883 20', null, 705, 50, 'South Carolina', 'Myrtle Beach',
        '816 90', 'Scofield', 306, 71, null, 35);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (172, to_date('16-Jan-20', 'DD-MON-RR'), to_date('16-Feb-20', 'DD-MON-RR'), '1', 839.6, 13, 57, '1',
        'montes.txt', 'Oregon', 'Salem', '611 87', null, 748, 64, 'Florida', 'Miami', '883 20', null, 705, 50, null,
        7);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (173, to_date('09-Feb-20', 'DD-MON-RR'), to_date('09-Mar-20', 'DD-MON-RR'), '1', 201.56, 12, 98, '0',
        'vestibulum_quam.txt', 'Massachusetts', 'Newton', '530 09', 'Summerview', 567, 6, 'Oregon', 'Salem',
        '611 87', null, 748, 64, null, 38);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (174, to_date('21-Mar-20', 'DD-MON-RR'), to_date('21-Apr-20', 'DD-MON-RR'), '0', 233.24, 15, 84, '0',
        'bibendum_morbi_non.txt', 'Florida', 'Pensacola', '355 91', 'Fairview', 589, 75, 'Massachusetts', 'Newton',
        '530 09', 'Summerview', 567, 6, null, 8);

insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (176, to_date('09-Apr-20', 'DD-MON-RR'), null, '0', 745.91, 19, 50, '0', 'sem.txt', 'California',
        'Los Angeles', '066 44', 'Brickson Park', 999, 3, 'Colorado', 'Englewood', '282 95', 'Nobel', 657, 9, null,
        12);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (177, to_date('21-Apr-20', 'DD-MON-RR'), null, '0', 212.32, 16, 78, '1', 'phasellus.txt', 'Colorado',
        'Denver', '531 13', null, 722, 96, 'California', 'Los Angeles', '066 44', 'Brickson Park', 999, 3,
        'ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in',
        73);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (178, to_date('14-Apr-20', 'DD-MON-RR'), null, '1', 690.3, 14, 70, '1', 'scelerisque.txt', 'Virginia',
        'Lynchburg', '099 90', 'Esch', 758, 88, 'Virginia', 'Lynchburg', '099 90', 'Esch', 758, 88, null, 10);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (179, to_date('01-Feb-20', 'DD-MON-RR'), to_date('01-Mar-20', 'DD-MON-RR'), '1', 949.33, 14, 100, '1',
        'nunc.txt', 'District of Columbia', 'Washington', '273 54', 'Sloan', 865, 32, 'District of Columbia',
        'Washington', '273 54', 'Sloan', 865, 32,
        'nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas',
        18);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (180, to_date('07-Jan-20', 'DD-MON-RR'), to_date('07-Feb-20', 'DD-MON-RR'), '1', 243.5, 17, 73, '0',
        'nulla_integer_pede.txt', 'Florida', 'Naples', '644 24', null, 231, 27, 'Florida', 'Naples', '644 24', null,
        231, 27, null, 40);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (181, to_date('06-Feb-20', 'DD-MON-RR'), to_date('06-Mar-20', 'DD-MON-RR'), '1', 623.41, 16, 67, '0',
        'felis_eu.txt', 'Florida', 'Lehigh Acres', '907 01', 'Florence', 647, 35, 'Florida', 'Lehigh Acres',
        '907 01', 'Florence', 647, 35, null, 14);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (182, to_date('11-Apr-20', 'DD-MON-RR'), null, '0', 305.33, 18, 69, '0', 'posuere_cubilia_curae.txt',
        'California', 'Orange', '257 04', 'Packers', 691, 29, 'California', 'Orange', '257 04', 'Packers', 691, 29,
        null, 15);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (183, to_date('03-Feb-20', 'DD-MON-RR'), to_date('03-Mar-20', 'DD-MON-RR'), '1', 367.13, 15, 115, '1',
        'congue.txt', 'Georgia', 'Atlanta', '139 87', 'Northfield', 934, 95, 'Georgia', 'Atlanta', '139 87',
        'Northfield', 934, 95, null, 78);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (184, to_date('17-Apr-20', 'DD-MON-RR'), null, '1', 203.82, 11, 71, '1', 'vestibulum_ac.txt', 'Texas',
        'Austin', '659 13', 'Comanche', 638, 78, 'Texas', 'Austin', '659 13', 'Comanche', 638, 78, null, 81);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (185, to_date('18-Apr-20', 'DD-MON-RR'), null, '0', 482.06, 10, 63, '1', 'vel.txt', 'Arizona', 'Phoenix',
        '005 17', 'Fallview', 511, null, 'Arizona', 'Phoenix', '005 17', 'Fallview', 511, null, null, 77);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (186, to_date('27-Mar-20', 'DD-MON-RR'), to_date('27-Apr-20', 'DD-MON-RR'), '0', 410.7, 12, 51, '1',
        'sagittis_dui.txt', 'California', 'Sacramento', '257 69', 'West', 843, null, 'California', 'Sacramento',
        '257 69', 'West', 843, null, null, 44);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (187, to_date('13-Jan-20', 'DD-MON-RR'), to_date('13-Feb-20', 'DD-MON-RR'), '0', 489.7, 11, 64, '1',
        'turpis_adipiscing_lorem.txt', 'Michigan', 'Detroit', '651 81', null, 714, 57, 'Michigan', 'Detroit',
        '651 81', null, 714, 57, 'non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla',
        15);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (188, to_date('23-Jan-20', 'DD-MON-RR'), to_date('23-Feb-20', 'DD-MON-RR'), '0', 857.52, 13, 104, '0',
        'in_est.txt', 'Iowa', 'Des Moines', '577 33', 'Tennyson', 166, null, 'Iowa', 'Des Moines', '577 33',
        'Tennyson', 166, null,
        'molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis', 56);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (189, to_date('25-Jan-20', 'DD-MON-RR'), to_date('25-Feb-20', 'DD-MON-RR'), '0', 464.13, 19, 62, '0',
        'vel.txt', 'Mississippi', 'Hattiesburg', '421 91', 'Chinook', 531, 35, 'Mississippi', 'Hattiesburg',
        '421 91', 'Chinook', 531, 35, null, 91);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (190, to_date('06-Feb-20', 'DD-MON-RR'), to_date('06-Mar-20', 'DD-MON-RR'), '0', 710.97, 18, 91, '1',
        'ut.txt', 'West Virginia', 'Huntington', '953 63', null, 169, 48, 'West Virginia', 'Huntington', '953 63',
        null, 169, 48, null, 77);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (191, to_date('24-Apr-20', 'DD-MON-RR'), null, '0', 359.17, 11, 88, '1', 'pede_morbi.txt', 'Alabama',
        'Birmingham', '691 85', 'Northwestern', 565, 41, 'California', 'Oceanside', '601 60', null, 515, 76,
        'ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed', 16);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (192, to_date('15-Mar-20', 'DD-MON-RR'), to_date('15-Apr-20', 'DD-MON-RR'), '1', 791.85, 16, 92, '1',
        'leo_maecenas.txt', 'Virginia', 'Herndon', '590 25', 'Magdeline', 839, 19, 'Alabama', 'Birmingham',
        '691 85', 'Northwestern', 565, 41, null, 7);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (193, to_date('26-Feb-20', 'DD-MON-RR'), to_date('26-Mar-20', 'DD-MON-RR'), '0', 449.0, 18, 95, '1',
        'justo_in_blandit.txt', 'Louisiana', 'Baton Rouge', '238 74', 'Sage', 351, null, 'Virginia', 'Herndon',
        '590 25', 'Magdeline', 839, 19, null, 37);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (194, to_date('28-Apr-20', 'DD-MON-RR'), null, '1', 509.7, 10, 99, '1', 'nec_nisi.txt', 'Utah',
        'Salt Lake City', '844 99', null, 603, 79, 'Louisiana', 'Baton Rouge', '238 74', 'Sage', 351, null, null,
        84);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (195, to_date('03-Apr-20', 'DD-MON-RR'), null, '1', 617.8, 13, 113, '1', 'nulla_mollis_molestie.txt',
        'Alaska', 'Anchorage', '038 93', 'Reindahl', 188, null, 'Utah', 'Salt Lake City', '844 99', null, 603, 79,
        'aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio',
        33);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (196, to_date('23-Apr-20', 'DD-MON-RR'), null, '1', 626.19, 12, 77, '0', 'turpis_a_pede.txt', 'California',
        'San Diego', '424 85', 'Vera', 510, null, 'Alaska', 'Anchorage', '038 93', 'Reindahl', 188, null, null, 15);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (197, to_date('12-Mar-20', 'DD-MON-RR'), to_date('12-Apr-20', 'DD-MON-RR'), '0', 914.17, 19, 89, '1',
        'ornare_imperdiet_sapien.txt', 'Florida', 'Miami', '329 86', null, 707, null, 'California', 'San Diego',
        '424 85', 'Vera', 510, null, null, 81);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (198, to_date('26-Apr-20', 'DD-MON-RR'), null, '1', 204.91, 17, 83, '1', 'eget_eleifend.txt', 'California',
        'Oceanside', '601 60', null, 515, 76, 'Florida', 'Miami', '329 86', null, 707, null, null, 92);

insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (200, to_date('15-Jan-20', 'DD-MON-RR'), to_date('15-Feb-20', 'DD-MON-RR'), '0', 415.72, 10, 82, '0',
        'aliquam_non.txt', 'Kentucky', 'Louisville', '350 88', 'Katie', 218, 51, 'Kentucky', 'Louisville', '350 88',
        'Katie', 218, 51, null, 41);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (201, to_date('17-Jan-20', 'DD-MON-RR'), to_date('17-Feb-20', 'DD-MON-RR'), '1', 605.13, 11, 67, '1',
        'augue_aliquam.txt', 'California', 'Berkeley', '932 63', 'Namekagon', 993, null, 'California', 'Berkeley',
        '932 63', 'Namekagon', 993, null, null, 33);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (202, to_date('25-Apr-20', 'DD-MON-RR'), null, '0', 803.57, 19, 81, '0', 'vehicula_condimentum.txt',
        'Nebraska', 'Omaha', '622 96', null, 839, 41, 'Nebraska', 'Omaha', '622 96', null, 839, 41, null, 92);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (203, to_date('25-Apr-20', 'DD-MON-RR'), null, '0', 202.46, 13, 94, '0', 'pede.txt', 'Illinois',
        'Springfield', '236 35', null, 586, 33, 'Illinois', 'Springfield', '236 35', null, 586, 33, null, 69);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (204, to_date('13-Feb-20', 'DD-MON-RR'), to_date('13-Mar-20', 'DD-MON-RR'), '1', 689.39, 17, 113, '1',
        'dapibus_at_diam.txt', 'Mississippi', 'Meridian', '269 78', null, 935, null, 'California', 'Santa Monica',
        '198 97', null, 181, null, null, 57);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (205, to_date('17-Apr-20', 'DD-MON-RR'), null, '1', 389.43, 14, 87, '1', 'augue_vestibulum_rutrum.txt',
        'Texas', 'Houston', '914 13', null, 139, 66, 'Mississippi', 'Meridian', '269 78', null, 935, null, null, 5);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (206, to_date('13-Mar-20', 'DD-MON-RR'), to_date('13-Apr-20', 'DD-MON-RR'), '0', 314.16, 15, 118, '1',
        'vulputate_elementum_nullam.txt', 'North Carolina', 'High Point', '097 89', 'Gerald', 805, 17, 'Texas',
        'Houston', '914 13', null, 139, 66, null, 53);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (207, to_date('29-Mar-20', 'DD-MON-RR'), to_date('29-Apr-20', 'DD-MON-RR'), '0', 706.68, 14, 97, '0',
        'tellus.txt', 'Michigan', 'Southfield', '566 02', 'Rutledge', 862, 33, 'North Carolina', 'High Point',
        '097 89', 'Gerald', 805, 17, null, 83);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (208, to_date('29-Mar-20', 'DD-MON-RR'), to_date('29-Apr-20', 'DD-MON-RR'), '1', 798.78, 14, 70, '1',
        'curae_nulla.txt', 'California', 'Santa Monica', '547 99', 'Arkansas', 508, 18, 'Michigan', 'Southfield',
        '566 02', 'Rutledge', 862, 33, null, 47);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (209, to_date('11-Feb-20', 'DD-MON-RR'), to_date('11-Mar-20', 'DD-MON-RR'), '0', 436.3, 17, 114, '0',
        'sapien_sapien_non.txt', 'Texas', 'Denton', '349 10', 'Talmadge', 431, 18, 'California', 'Santa Monica',
        '547 99', 'Arkansas', 508, 18, null, 17);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (210, to_date('01-Apr-20', 'DD-MON-RR'), null, '1', 421.86, 14, 69, '0', 'felis_fusce_posuere.txt',
        'Pennsylvania', 'Harrisburg', '190 25', 'Upham', 332, 68, 'Texas', 'Denton', '349 10', 'Talmadge', 431, 18,
        null, 9);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (211, to_date('08-Jan-20', 'DD-MON-RR'), to_date('08-Feb-20', 'DD-MON-RR'), '1', 244.18, 20, 104, '1',
        'fusce_lacus_purus.txt', 'California', 'Santa Monica', '198 97', null, 181, null, 'Pennsylvania',
        'Harrisburg', '190 25', 'Upham', 332, 68, null, 90);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (212, to_date('22-Feb-20', 'DD-MON-RR'), to_date('22-Mar-20', 'DD-MON-RR'), '0', 774.07, 12, 68, '0',
        'lobortis.txt', 'Massachusetts', 'Springfield', '020 38', 'Maryland', 759, 75, 'Massachusetts',
        'Springfield', '020 38', 'Maryland', 759, 75, null, 92);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (213, to_date('27-Feb-20', 'DD-MON-RR'), to_date('27-Mar-20', 'DD-MON-RR'), '1', 384.86, 20, 70, '0',
        'vitae_quam.txt', 'New York', 'Yonkers', '058 72', null, 314, 82, 'New York', 'Yonkers', '058 72', null,
        314, 82, 'nunc nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor', 44);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (214, to_date('14-Mar-20', 'DD-MON-RR'), to_date('14-Apr-20', 'DD-MON-RR'), '0', 799.26, 20, 56, '0',
        'amet.txt', 'Massachusetts', 'Boston', '330 61', 'Warbler', 213, 33, 'Massachusetts', 'Boston', '330 61',
        'Warbler', 213, 33, null, 30);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (215, to_date('27-Mar-20', 'DD-MON-RR'), to_date('27-Apr-20', 'DD-MON-RR'), '0', 893.66, 17, 90, '1',
        'nisl.txt', 'Oklahoma', 'Edmond', '491 40', null, 130, null, 'Oklahoma', 'Edmond', '491 40', null, 130,
        null, null, 91);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (216, to_date('23-Apr-20', 'DD-MON-RR'), null, '1', 352.82, 10, 52, '1', 'in_felis_donec.txt', 'Minnesota',
        'Saint Paul', '021 60', 'Logan', 205, 31, 'Minnesota', 'Saint Paul', '021 60', 'Logan', 205, 31, null, 25);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (217, to_date('15-Mar-20', 'DD-MON-RR'), to_date('15-Apr-20', 'DD-MON-RR'), '0', 726.99, 15, 51, '1',
        'nam.txt', 'California', 'Sacramento', '832 06', 'Lawn', 148, 75, 'California', 'Sacramento', '832 06',
        'Lawn', 148, 75,
        'varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper',
        88);

insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (219, to_date('21-Apr-20', 'DD-MON-RR'), null, '1', 419.91, 20, 61, '1', 'sed_tristique.txt', 'Alabama',
        'Huntsville', '527 31', 'Lillian', 619, 24, 'Alabama', 'Huntsville', '527 31', 'Lillian', 619, 24,
        'posuere cubilia curae nulla dapibus dolor vel est donec odio', 80);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (220, to_date('24-Jan-20', 'DD-MON-RR'), to_date('24-Feb-20', 'DD-MON-RR'), '1', 677.78, 19, 120, '1',
        'cubilia_curae_donec.txt', 'West Virginia', 'Huntington', '769 54', 'Banding', 237, null, 'West Virginia',
        'Huntington', '769 54', 'Banding', 237, null, null, 20);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (221, to_date('02-Jan-20', 'DD-MON-RR'), to_date('02-Feb-20', 'DD-MON-RR'), '0', 794.54, 13, 98, '1',
        'in.txt', 'New York', 'New York City', '468 97', 'Cambridge', 251, 32, 'New York', 'New York City',
        '468 97', 'Cambridge', 251, 32, null, 14);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (222, to_date('19-Jan-20', 'DD-MON-RR'), to_date('19-Feb-20', 'DD-MON-RR'), '1', 470.14, 20, 109, '1',
        'cubilia.txt', 'Alabama', 'Mobile', '686 97', 'Troy', 339, null, 'Alabama', 'Mobile', '686 97', 'Troy', 339,
        null,
        'duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo',
        15);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (223, to_date('09-Apr-20', 'DD-MON-RR'), null, '1', 728.5, 20, 67, '0', 'nulla_nisl_nunc.txt', 'Texas',
        'Amarillo', '028 78', null, 295, 24, 'Missouri', 'Lees Summit', '428 26', 'Bashford', 500, 30, null, 72);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (224, to_date('11-Apr-20', 'DD-MON-RR'), null, '0', 236.26, 17, 107, '0', 'varius_ut_blandit.txt',
        'Illinois', 'Chicago', '072 10', 'Superior', 687, null, 'Texas', 'Amarillo', '028 78', null, 295, 24, null,
        7);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (225, to_date('10-Feb-20', 'DD-MON-RR'), to_date('10-Mar-20', 'DD-MON-RR'), '0', 760.03, 13, 65, '0',
        'in.txt', 'Florida', 'Punta Gorda', '400 07', null, 230, null, 'Illinois', 'Chicago', '072 10', 'Superior',
        687, null, null, 25);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (226, to_date('11-Apr-20', 'DD-MON-RR'), null, '1', 342.45, 20, 56, '1', 'aliquam.txt', 'Texas', 'Dallas',
        '908 67', 'Fallview', 235, 57, 'Florida', 'Punta Gorda', '400 07', null, 230, null, null, 81);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (227, to_date('03-Apr-20', 'DD-MON-RR'), null, '1', 480.33, 19, 90, '1', 'sed.txt', 'Florida',
        'Jacksonville', '852 48', 'Schmedeman', 284, 34, 'Texas', 'Dallas', '908 67', 'Fallview', 235, 57, null,
        46);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (228, to_date('19-Feb-20', 'DD-MON-RR'), to_date('19-Mar-20', 'DD-MON-RR'), '1', 968.88, 18, 65, '1',
        'montes_nascetur_ridiculus.txt', 'Oklahoma', 'Oklahoma City', '565 90', 'Pleasure', 755, null, 'Florida',
        'Jacksonville', '852 48', 'Schmedeman', 284, 34, null, 17);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (229, to_date('11-Apr-20', 'DD-MON-RR'), null, '0', 637.71, 17, 71, '1', 'egestas_metus_aenean.txt',
        'Pennsylvania', 'Harrisburg', '905 48', 'Hauk', 914, 8, 'Oklahoma', 'Oklahoma City', '565 90', 'Pleasure',
        755, null, null, 62);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (230, to_date('26-Apr-20', 'DD-MON-RR'), null, '1', 724.9, 19, 56, '1', 'dis.txt', 'Missouri', 'Lees Summit',
        '428 26', 'Bashford', 500, 30, 'Pennsylvania', 'Harrisburg', '905 48', 'Hauk', 914, 8, null, 65);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (231, to_date('20-Apr-20', 'DD-MON-RR'), null, '1', 761.8, 18, 116, '1', 'mollis_molestie.txt', 'Florida',
        'Tallahassee', '754 20', 'Del Sol', 405, 14, 'Florida', 'Tallahassee', '754 20', 'Del Sol', 405, 14, null,
        8);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (232, to_date('15-Jan-20', 'DD-MON-RR'), to_date('15-Feb-20', 'DD-MON-RR'), '1', 317.41, 13, 117, '1',
        'aliquam_non.txt', 'Alabama', 'Birmingham', '042 77', 'Helena', 777, 34, 'Alabama', 'Birmingham', '042 77',
        'Helena', 777, 34, null, 84);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (233, to_date('17-Feb-20', 'DD-MON-RR'), to_date('17-Mar-20', 'DD-MON-RR'), '0', 947.28, 20, 93, '1',
        'urna_ut_tellus.txt', 'Kansas', 'Topeka', '858 88', 'Drewry', 709, 30, 'Kansas', 'Topeka', '858 88',
        'Drewry', 709, 30, null, 18);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (234, to_date('22-Jan-20', 'DD-MON-RR'), to_date('22-Feb-20', 'DD-MON-RR'), '1', 950.57, 16, 81, '1',
        'convallis_duis_consequat.txt', 'New York', 'Albany', '945 13', null, 265, 45, 'New York', 'Albany',
        '945 13', null, 265, 45, null, 37);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (235, to_date('28-Feb-20', 'DD-MON-RR'), to_date('28-Mar-20', 'DD-MON-RR'), '0', 772.09, 19, 89, '0',
        'platea.txt', 'California', 'Van Nuys', '028 48', null, 805, 36, 'California', 'Van Nuys', '028 48', null,
        805, 36, null, 72);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (236, to_date('27-Mar-20', 'DD-MON-RR'), to_date('27-Apr-20', 'DD-MON-RR'), '1', 595.22, 13, 98, '1',
        'id_massa_id.txt', 'Utah', 'Salt Lake City', '957 74', 'Old Gate', 790, 28, 'Utah', 'Salt Lake City',
        '957 74', 'Old Gate', 790, 28, null, 58);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (237, to_date('22-Mar-20', 'DD-MON-RR'), to_date('22-Apr-20', 'DD-MON-RR'), '1', 720.57, 12, 102, '1',
        'lacinia_nisi_venenatis.txt', 'Arizona', 'Tucson', '179 57', 'Burrows', 272, null, 'Arizona', 'Tucson',
        '179 57', 'Burrows', 272, null, null, 6);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (238, to_date('03-Mar-20', 'DD-MON-RR'), to_date('03-Apr-20', 'DD-MON-RR'), '0', 495.59, 10, 90, '1',
        'penatibus_et.txt', 'Iowa', 'Des Moines', '392 86', 'Westend', 925, null, 'Florida', 'Pensacola', '355 91',
        'Fairview', 589, 75, null, 82);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (239, to_date('22-Jan-20', 'DD-MON-RR'), to_date('22-Feb-20', 'DD-MON-RR'), '1', 674.65, 18, 58, '0',
        'malesuada.txt', 'Missouri', 'Kansas City', '508 15', 'Elmside', 575, 58, 'Iowa', 'Des Moines', '392 86',
        'Westend', 925, null, null, 90);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (240, to_date('10-Apr-20', 'DD-MON-RR'), null, '0', 751.84, 15, 99, '0', 'mauris.txt', 'Connecticut',
        'Hartford', '021 89', 'Arrowood', 250, null, 'Missouri', 'Kansas City', '508 15', 'Elmside', 575, 58, null,
        78);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (241, to_date('11-Mar-20', 'DD-MON-RR'), to_date('11-Apr-20', 'DD-MON-RR'), '0', 241.63, 15, 51, '0',
        'a_odio_in.txt', 'South Carolina', 'Myrtle Beach', '816 90', 'Scofield', 306, 71, 'Connecticut', 'Hartford',
        '021 89', 'Arrowood', 250, null, null, 28);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (242, to_date('20-Feb-20', 'DD-MON-RR'), to_date('20-Mar-20', 'DD-MON-RR'), '0', 233.68, 14, 61, '0',
        'sit.txt', 'Florida', 'Miami', '883 20', null, 705, 50, 'South Carolina', 'Myrtle Beach', '816 90',
        'Scofield', 306, 71, null, 39);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (243, to_date('08-Apr-20', 'DD-MON-RR'), null, '0', 469.8, 18, 65, '1', 'magna_bibendum_imperdiet.txt',
        'Oregon', 'Salem', '611 87', null, 748, 64, 'Florida', 'Miami', '883 20', null, 705, 50, null, 8);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (244, to_date('24-Mar-20', 'DD-MON-RR'), to_date('24-Apr-20', 'DD-MON-RR'), '1', 396.83, 15, 79, '1',
        'metus_aenean.txt', 'Massachusetts', 'Newton', '530 09', 'Summerview', 567, 6, 'Oregon', 'Salem', '611 87',
        null, 748, 64, 'suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla', 92);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (245, to_date('29-Feb-20', 'DD-MON-RR'), to_date('29-Mar-20', 'DD-MON-RR'), '1', 801.05, 16, 83, '0',
        'cursus.txt', 'Florida', 'Pensacola', '355 91', 'Fairview', 589, 75, 'Massachusetts', 'Newton', '530 09',
        'Summerview', 567, 6,
        'aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet', 46);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (246, to_date('29-Apr-20', 'DD-MON-RR'), null, '1', 414.96, 14, 119, '0', 'ante_vivamus_tortor.txt',
        'Colorado', 'Englewood', '282 95', 'Nobel', 657, 9, 'Colorado', 'Englewood', '282 95', 'Nobel', 657, 9,
        null, 25);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (247, to_date('22-Jan-20', 'DD-MON-RR'), to_date('22-Feb-20', 'DD-MON-RR'), '0', 623.13, 17, 62, '0',
        'tincidunt_nulla_mollis.txt', 'California', 'Los Angeles', '066 44', 'Brickson Park', 999, 3, 'California',
        'Los Angeles', '066 44', 'Brickson Park', 999, 3, null, 18);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (248, to_date('19-Feb-20', 'DD-MON-RR'), to_date('19-Mar-20', 'DD-MON-RR'), '0', 493.0, 11, 119, '0',
        'id_massa.txt', 'Colorado', 'Denver', '531 13', null, 722, 96, 'Colorado', 'Denver', '531 13', null, 722,
        96, null, 75);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (249, to_date('25-Feb-20', 'DD-MON-RR'), to_date('25-Mar-20', 'DD-MON-RR'), '0', 357.16, 18, 58, '1',
        'orci_luctus.txt', 'Virginia', 'Lynchburg', '099 90', 'Esch', 758, 88, 'Virginia', 'Lynchburg', '099 90',
        'Esch', 758, 88, null, 94);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (250, to_date('25-Feb-20', 'DD-MON-RR'), to_date('25-Mar-20', 'DD-MON-RR'), '1', 981.5, 15, 71, '0',
        'ornare_consequat.txt', 'District of Columbia', 'Washington', '273 54', 'Sloan', 865, 32,
        'District of Columbia', 'Washington', '273 54', 'Sloan', 865, 32, null, 27);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (251, to_date('12-Mar-20', 'DD-MON-RR'), to_date('12-Apr-20', 'DD-MON-RR'), '1', 688.56, 13, 105, '0',
        'pellentesque.txt', 'Florida', 'Naples', '644 24', null, 231, 27, 'Florida', 'Naples', '644 24', null, 231,
        27,
        'dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel',
        36);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (252, to_date('08-Feb-20', 'DD-MON-RR'), to_date('08-Mar-20', 'DD-MON-RR'), '1', 564.22, 13, 93, '0',
        'orci_vehicula_condimentum.txt', 'Florida', 'Lehigh Acres', '907 01', 'Florence', 647, 35, 'Florida',
        'Lehigh Acres', '907 01', 'Florence', 647, 35,
        'viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero',
        91);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (253, to_date('02-Feb-20', 'DD-MON-RR'), to_date('02-Mar-20', 'DD-MON-RR'), '1', 652.72, 16, 92, '0',
        'amet_sem_fusce.txt', 'California', 'Orange', '257 04', 'Packers', 691, 29, 'California', 'Orange',
        '257 04', 'Packers', 691, 29, null, 74);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (254, to_date('21-Mar-20', 'DD-MON-RR'), to_date('21-Apr-20', 'DD-MON-RR'), '0', 514.01, 19, 112, '0',
        'accumsan.txt', 'Georgia', 'Atlanta', '139 87', 'Northfield', 934, 95, 'West Virginia', 'Huntington',
        '953 63', null, 169, 48, null, 10);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (255, to_date('19-Mar-20', 'DD-MON-RR'), to_date('19-Apr-20', 'DD-MON-RR'), '0', 592.4, 19, 113, '1',
        'odio.txt', 'Texas', 'Austin', '659 13', 'Comanche', 638, 78, 'Georgia', 'Atlanta', '139 87', 'Northfield',
        934, 95, null, 90);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (256, to_date('02-Apr-20', 'DD-MON-RR'), null, '1', 870.6, 11, 92, '1', 'ut.txt', 'Arizona', 'Phoenix',
        '005 17', 'Fallview', 511, null, 'Texas', 'Austin', '659 13', 'Comanche', 638, 78, null, 15);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (257, to_date('19-Feb-20', 'DD-MON-RR'), to_date('19-Mar-20', 'DD-MON-RR'), '0', 415.26, 11, 95, '1',
        'ipsum_aliquam.txt', 'California', 'Sacramento', '257 69', 'West', 843, null, 'Arizona', 'Phoenix',
        '005 17', 'Fallview', 511, null,
        'mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet', 25);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (258, to_date('16-Mar-20', 'DD-MON-RR'), to_date('16-Apr-20', 'DD-MON-RR'), '0', 385.9, 15, 94, '1',
        'ipsum_praesent.txt', 'Michigan', 'Detroit', '651 81', null, 714, 57, 'California', 'Sacramento', '257 69',
        'West', 843, null, null, 65);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (259, to_date('06-Feb-20', 'DD-MON-RR'), to_date('06-Mar-20', 'DD-MON-RR'), '0', 268.17, 18, 64, '1',
        'nonummy_integer_non.txt', 'Iowa', 'Des Moines', '577 33', 'Tennyson', 166, null, 'Michigan', 'Detroit',
        '651 81', null, 714, 57, null, 80);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (260, to_date('04-Mar-20', 'DD-MON-RR'), to_date('04-Apr-20', 'DD-MON-RR'), '1', 538.18, 15, 103, '1',
        'imperdiet_nullam_orci.txt', 'Mississippi', 'Hattiesburg', '421 91', 'Chinook', 531, 35, 'Iowa',
        'Des Moines', '577 33', 'Tennyson', 166, null,
        'diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere',
        23);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (261, to_date('21-Jan-20', 'DD-MON-RR'), to_date('21-Feb-20', 'DD-MON-RR'), '0', 201.34, 10, 90, '0',
        'mus_etiam_vel.txt', 'West Virginia', 'Huntington', '953 63', null, 169, 48, 'Mississippi', 'Hattiesburg',
        '421 91', 'Chinook', 531, 35,
        'consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam', 20);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (262, to_date('03-Apr-20', 'DD-MON-RR'), null, '1', 509.42, 16, 57, '1', 'consectetuer.txt', 'Alabama',
        'Birmingham', '691 85', 'Northwestern', 565, 41, 'Alabama', 'Birmingham', '691 85', 'Northwestern', 565, 41,
        null, 16);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (263, to_date('24-Feb-20', 'DD-MON-RR'), to_date('24-Mar-20', 'DD-MON-RR'), '1', 984.7, 15, 68, '0',
        'vulputate.txt', 'Virginia', 'Herndon', '590 25', 'Magdeline', 839, 19, 'Virginia', 'Herndon', '590 25',
        'Magdeline', 839, 19, null, 24);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (264, to_date('23-Jan-20', 'DD-MON-RR'), to_date('23-Feb-20', 'DD-MON-RR'), '0', 956.92, 11, 82, '1',
        'mauris.txt', 'Louisiana', 'Baton Rouge', '238 74', 'Sage', 351, null, 'Louisiana', 'Baton Rouge', '238 74',
        'Sage', 351, null, null, 65);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (265, to_date('10-Jan-20', 'DD-MON-RR'), to_date('10-Feb-20', 'DD-MON-RR'), '0', 811.46, 16, 107, '0',
        'lobortis_convallis_tortor.txt', 'Utah', 'Salt Lake City', '844 99', null, 603, 79, 'Utah',
        'Salt Lake City', '844 99', null, 603, 79, null, 94);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (266, to_date('23-Jan-20', 'DD-MON-RR'), to_date('23-Feb-20', 'DD-MON-RR'), '1', 342.26, 17, 101, '0',
        'volutpat_sapien.txt', 'Alaska', 'Anchorage', '038 93', 'Reindahl', 188, null, 'Alaska', 'Anchorage',
        '038 93', 'Reindahl', 188, null, null, 23);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (267, to_date('25-Jan-20', 'DD-MON-RR'), to_date('25-Feb-20', 'DD-MON-RR'), '0', 548.71, 15, 110, '1',
        'nulla_elit_ac.txt', 'California', 'San Diego', '424 85', 'Vera', 510, null, 'Illinois', 'Springfield',
        '236 35', null, 586, 33, null, 20);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (268, to_date('05-Mar-20', 'DD-MON-RR'), to_date('05-Apr-20', 'DD-MON-RR'), '0', 944.81, 20, 65, '0',
        'quam.txt', 'Florida', 'Miami', '329 86', null, 707, null, 'California', 'San Diego', '424 85', 'Vera', 510,
        null, null, 12);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (269, to_date('05-Feb-20', 'DD-MON-RR'), to_date('05-Mar-20', 'DD-MON-RR'), '1', 775.39, 11, 103, '0',
        'velit_nec_nisi.txt', 'California', 'Oceanside', '601 60', null, 515, 76, 'Florida', 'Miami', '329 86',
        null, 707, null, null, 7);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (270, to_date('22-Apr-20', 'DD-MON-RR'), null, '0', 976.52, 14, 76, '0', 'tellus_nulla.txt', 'Maryland',
        'Baltimore', '273 55', null, 804, 75, 'California', 'Oceanside', '601 60', null, 515, 76, null, 62);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (271, to_date('15-Apr-20', 'DD-MON-RR'), null, '1', 724.47, 19, 71, '1', 'risus_auctor_sed.txt', 'Kentucky',
        'Louisville', '350 88', 'Katie', 218, 51, 'Maryland', 'Baltimore', '273 55', null, 804, 75, null, 85);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (272, to_date('22-Apr-20', 'DD-MON-RR'), null, '0', 725.61, 20, 58, '1', 'et_commodo.txt', 'California',
        'Berkeley', '932 63', 'Namekagon', 993, null, 'Kentucky', 'Louisville', '350 88', 'Katie', 218, 51, null,
        41);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (273, to_date('30-Mar-20', 'DD-MON-RR'), to_date('30-Apr-20', 'DD-MON-RR'), '0', 885.58, 19, 59, '1',
        'eu.txt', 'Nebraska', 'Omaha', '622 96', null, 839, 41, 'California', 'Berkeley', '932 63', 'Namekagon',
        993, null, null, 35);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (274, to_date('23-Mar-20', 'DD-MON-RR'), to_date('23-Apr-20', 'DD-MON-RR'), '1', 403.04, 15, 116, '1',
        'elementum.txt', 'Illinois', 'Springfield', '236 35', null, 586, 33, 'Nebraska', 'Omaha', '622 96', null,
        839, 41, null, 62);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (275, to_date('29-Jan-20', 'DD-MON-RR'), to_date('29-Feb-20', 'DD-MON-RR'), '0', 363.63, 11, 84, '0',
        'congue_risus_semper.txt', 'Mississippi', 'Meridian', '269 78', null, 935, null, 'Mississippi', 'Meridian',
        '269 78', null, 935, null, null, 5);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (276, to_date('08-Mar-20', 'DD-MON-RR'), to_date('08-Apr-20', 'DD-MON-RR'), '1', 484.0, 20, 52, '1',
        'duis_bibendum.txt', 'Texas', 'Houston', '914 13', null, 139, 66, 'Texas', 'Houston', '914 13', null, 139,
        66, null, 67);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (277, to_date('08-Jan-20', 'DD-MON-RR'), to_date('08-Feb-20', 'DD-MON-RR'), '1', 884.04, 12, 78, '0',
        'et_ultrices_posuere.txt', 'North Carolina', 'High Point', '097 89', 'Gerald', 805, 17, 'North Carolina',
        'High Point', '097 89', 'Gerald', 805, 17,
        'porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh',
        41);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (278, to_date('01-Jan-20', 'DD-MON-RR'), to_date('01-Feb-20', 'DD-MON-RR'), '1', 401.38, 12, 115, '1',
        'duis_aliquam.txt', 'Michigan', 'Southfield', '566 02', 'Rutledge', 862, 33, 'Michigan', 'Southfield',
        '566 02', 'Rutledge', 862, 33, null, 63);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (279, to_date('27-Feb-20', 'DD-MON-RR'), to_date('27-Mar-20', 'DD-MON-RR'), '0', 507.02, 11, 76, '0',
        'odio_odio_elementum.txt', 'California', 'Santa Monica', '547 99', 'Arkansas', 508, 18, 'California',
        'Santa Monica', '547 99', 'Arkansas', 508, 18, null, 55);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (280, to_date('20-Jan-20', 'DD-MON-RR'), to_date('20-Feb-20', 'DD-MON-RR'), '0', 868.77, 14, 89, '1',
        'vitae_ipsum.txt', 'Texas', 'Denton', '349 10', 'Talmadge', 431, 18, 'Texas', 'Denton', '349 10',
        'Talmadge', 431, 18, null, 74);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (281, to_date('26-Apr-20', 'DD-MON-RR'), null, '0', 840.08, 10, 51, '1', 'tellus.txt', 'Pennsylvania',
        'Harrisburg', '190 25', 'Upham', 332, 68, 'Pennsylvania', 'Harrisburg', '190 25', 'Upham', 332, 68,
        'vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient', 89);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (282, to_date('08-Mar-20', 'DD-MON-RR'), to_date('08-Apr-20', 'DD-MON-RR'), '1', 873.99, 20, 57, '1',
        'aliquet_pulvinar.txt', 'California', 'Santa Monica', '198 97', null, 181, null, 'California',
        'Santa Monica', '198 97', null, 181, null, null, 81);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (283, to_date('04-Mar-20', 'DD-MON-RR'), to_date('04-Apr-20', 'DD-MON-RR'), '0', 728.79, 15, 120, '0',
        'blandit_nam_nulla.txt', 'Massachusetts', 'Springfield', '020 38', 'Maryland', 759, 75, 'Alabama',
        'Huntsville', '527 31', 'Lillian', 619, 24, null, 41);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (284, to_date('27-Apr-20', 'DD-MON-RR'), null, '1', 758.42, 14, 66, '1', 'ultrices.txt', 'New York',
        'Yonkers', '058 72', null, 314, 82, 'Massachusetts', 'Springfield', '020 38', 'Maryland', 759, 75,
        'est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim',
        53);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (285, to_date('14-Jan-20', 'DD-MON-RR'), to_date('14-Feb-20', 'DD-MON-RR'), '1', 241.17, 20, 100, '0',
        'luctus_tincidunt.txt', 'Massachusetts', 'Boston', '330 61', 'Warbler', 213, 33, 'New York', 'Yonkers',
        '058 72', null, 314, 82, null, 42);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (286, to_date('15-Feb-20', 'DD-MON-RR'), to_date('15-Mar-20', 'DD-MON-RR'), '0', 582.86, 19, 59, '1',
        'mauris.txt', 'Oklahoma', 'Edmond', '491 40', null, 130, null, 'Massachusetts', 'Boston', '330 61',
        'Warbler', 213, 33, null, 39);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (287, to_date('27-Mar-20', 'DD-MON-RR'), to_date('27-Apr-20', 'DD-MON-RR'), '1', 689.89, 17, 81, '0',
        'ante.txt', 'Minnesota', 'Saint Paul', '021 60', 'Logan', 205, 31, 'Oklahoma', 'Edmond', '491 40', null,
        130, null, null, 23);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (288, to_date('30-Mar-20', 'DD-MON-RR'), to_date('30-Apr-20', 'DD-MON-RR'), '0', 679.5, 14, 57, '0',
        'tincidunt_eu.txt', 'California', 'Sacramento', '832 06', 'Lawn', 148, 75, 'Minnesota', 'Saint Paul',
        '021 60', 'Logan', 205, 31, 'vehicula consequat morbi a ipsum integer a nibh in quis', 23);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (289, to_date('14-Jan-20', 'DD-MON-RR'), to_date('14-Feb-20', 'DD-MON-RR'), '1', 913.1, 19, 117, '0',
        'euismod.txt', 'District of Columbia', 'Washington', '627 33', 'Dwight', 216, 56, 'California',
        'Sacramento', '832 06', 'Lawn', 148, 75, null, 73);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (290, to_date('05-Apr-20', 'DD-MON-RR'), null, '1', 385.15, 11, 98, '0', 'posuere_cubilia_curae.txt',
        'Alabama', 'Huntsville', '527 31', 'Lillian', 619, 24, 'District of Columbia', 'Washington', '627 33',
        'Dwight', 216, 56,
        'porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec semper',
        29);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (291, to_date('23-Feb-20', 'DD-MON-RR'), to_date('23-Mar-20', 'DD-MON-RR'), '0', 817.31, 13, 112, '0',
        'porttitor.txt', 'West Virginia', 'Huntington', '769 54', 'Banding', 237, null, 'West Virginia',
        'Huntington', '769 54', 'Banding', 237, null, null, 44);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (292, to_date('09-Apr-20', 'DD-MON-RR'), null, '0', 884.41, 12, 89, '1', 'vestibulum_aliquet.txt',
        'New York', 'New York City', '468 97', 'Cambridge', 251, 32, 'New York', 'New York City', '468 97',
        'Cambridge', 251, 32, null, 36);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (293, to_date('27-Mar-20', 'DD-MON-RR'), to_date('27-Apr-20', 'DD-MON-RR'), '0', 601.5, 11, 110, '1',
        'id_nulla_ultrices.txt', 'Alabama', 'Mobile', '686 97', 'Troy', 339, null, 'Alabama', 'Mobile', '686 97',
        'Troy', 339, null, null, 25);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (294, to_date('06-Mar-20', 'DD-MON-RR'), to_date('06-Apr-20', 'DD-MON-RR'), '0', 912.7, 12, 58, '1',
        'consequat.txt', 'Texas', 'Amarillo', '028 78', null, 295, 24, 'Texas', 'Amarillo', '028 78', null, 295, 24,
        null, 40);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (295, to_date('16-Feb-20', 'DD-MON-RR'), to_date('16-Mar-20', 'DD-MON-RR'), '1', 224.63, 16, 106, '0',
        'pretium.txt', 'Illinois', 'Chicago', '072 10', 'Superior', 687, null, 'Illinois', 'Chicago', '072 10',
        'Superior', 687, null, null, 9);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (296, to_date('27-Mar-20', 'DD-MON-RR'), to_date('27-Apr-20', 'DD-MON-RR'), '1', 534.74, 17, 75, '1',
        'aliquam_sit_amet.txt', 'Florida', 'Punta Gorda', '400 07', null, 230, null, 'Florida', 'Punta Gorda',
        '400 07', null, 230, null, null, 47);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (297, to_date('30-Mar-20', 'DD-MON-RR'), to_date('30-Apr-20', 'DD-MON-RR'), '1', 500.27, 13, 76, '0',
        'in_magna.txt', 'Texas', 'Dallas', '908 67', 'Fallview', 235, 57, 'Texas', 'Dallas', '908 67', 'Fallview',
        235, 57, null, 53);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (298, to_date('04-Jan-20', 'DD-MON-RR'), to_date('04-Feb-20', 'DD-MON-RR'), '0', 937.78, 13, 51, '1',
        'sed_sagittis_nam.txt', 'Florida', 'Jacksonville', '852 48', 'Schmedeman', 284, 34, 'Florida',
        'Jacksonville', '852 48', 'Schmedeman', 284, 34, null, 42);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (299, to_date('15-Mar-20', 'DD-MON-RR'), to_date('15-Apr-20', 'DD-MON-RR'), '0', 219.05, 16, 99, '0',
        'cum_sociis_natoque.txt', 'Oklahoma', 'Oklahoma City', '565 90', 'Pleasure', 755, null, 'Oklahoma',
        'Oklahoma City', '565 90', 'Pleasure', 755, null, null, 19);

insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (48, to_date('15-Jan-20', 'DD-MON-RR'), to_date('15-Feb-20', 'DD-MON-RR'), '0', 415.72, 10, 82, '0',
        'aliquam_non.txt', 'Kentucky', 'Louisville', '350 88', 'Katie', 218, 51, 'Kentucky', 'Louisville', '350 88',
        'Katie', 218, 51, null, 41);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (73, to_date('17-Jan-20', 'DD-MON-RR'), to_date('17-Feb-20', 'DD-MON-RR'), '1', 605.13, 11, 67, '1',
        'augue_aliquam.txt', 'California', 'Berkeley', '932 63', 'Namekagon', 993, null, 'California', 'Berkeley',
        '932 63', 'Namekagon', 993, null, null, 33);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (104, to_date('25-Apr-20', 'DD-MON-RR'), null, '0', 803.57, 19, 81, '0', 'vehicula_condimentum.txt',
        'Nebraska', 'Omaha', '622 96', null, 839, 41, 'Nebraska', 'Omaha', '622 96', null, 839, 41, null, 92);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (175, to_date('25-Apr-20', 'DD-MON-RR'), null, '0', 202.46, 13, 94, '0', 'pede.txt', 'Illinois',
        'Springfield', '236 35', null, 586, 33, 'Illinois', 'Springfield', '236 35', null, 586, 33, null, 69);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (199, to_date('13-Feb-20', 'DD-MON-RR'), to_date('13-Mar-20', 'DD-MON-RR'), '1', 689.39, 17, 113, '1',
        'dapibus_at_diam.txt', 'Mississippi', 'Meridian', '269 78', null, 935, null, 'California', 'Santa Monica',
        '198 97', null, 181, null, null, 57);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (218, to_date('13-Feb-20', 'DD-MON-RR'), to_date('13-Mar-20', 'DD-MON-RR'), '1', 689.39, 17, 113, '1',
        'dapibus_at_diam.txt', 'Mississippi', 'Meridian', '269 78', null, 935, null, 'California', 'Santa Monica',
        '198 97', null, 181, null, null, 57);
insert into OBJEDNAVKA (ID_OBJEDNAVKA, DATUM_OBJEDNANI, DATUM_DORUCENI, DORUCENO, CENA_PRODUKTU, CENA_BALNE,
                        CENA_POSTOVNE, ZAPLACENO, FAKTURA, STAT_DORUCOVACI, MESTO_DORUCOVACI, PSC_DORUCOVACI,
                        ULICE_DORUCOVACI, CISLO_POPISNE_DORUCOVACI, CISLO_ORIENTACNI_DORUCOVACI, STAT_FAKTURACNI,
                        MESTO_FAKTURACNI, PSC_FAKTURACNI, ULICE_FAKTURACNI, CISLO_POPISNE_FAKTURACNI,
                        CISLO_ORIENTACNI_FAKTURACNI, POZNAMKA, ID_UZIVATEL)
values (300, to_date('13-Mar-20', 'DD-MON-RR'), to_date('13-Apr-20', 'DD-MON-RR'), '0', 314.16, 15, 118, '1',
        'vulputate_elementum_nullam.txt', 'North Carolina', 'High Point', '097 89', 'Gerald', 805, 17, 'Texas',
        'Houston', '914 13', null, 139, 66, null, 53);

commit;


--300 zaznamu polozka

insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (1, 5, 235);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (2, 4, 127);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (3, 10, 289);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (4, 8, 199);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (5, 6, 235);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (6, 2, 139);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (7, 4, 83);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (8, 9, 242);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (9, 1, 32);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (10, 1, 300);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (11, 1, 192);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (12, 10, 34);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (13, 10, 243);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (14, 7, 187);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (15, 7, 89);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (16, 5, 46);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (17, 6, 285);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (18, 8, 78);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (19, 3, 285);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (20, 8, 267);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (21, 8, 282);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (22, 9, 10);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (23, 6, 74);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (24, 2, 207);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (25, 4, 241);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (26, 5, 48);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (27, 2, 56);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (28, 8, 91);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (29, 2, 218);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (30, 3, 110);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (31, 9, 30);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (32, 7, 283);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (33, 7, 7);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (34, 2, 246);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (35, 6, 259);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (36, 2, 250);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (37, 2, 278);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (38, 2, 242);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (39, 2, 237);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (40, 4, 156);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (41, 9, 168);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (42, 1, 300);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (43, 10, 291);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (44, 1, 204);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (45, 9, 226);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (46, 6, 98);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (47, 3, 228);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (48, 6, 77);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (49, 6, 136);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (50, 9, 257);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (51, 9, 251);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (52, 10, 140);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (53, 4, 115);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (54, 3, 166);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (55, 3, 56);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (56, 9, 284);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (57, 9, 137);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (58, 3, 233);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (59, 5, 86);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (60, 6, 234);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (61, 8, 227);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (62, 8, 68);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (63, 8, 24);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (64, 10, 172);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (65, 1, 222);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (66, 1, 43);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (67, 4, 230);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (68, 2, 214);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (69, 9, 212);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (70, 8, 134);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (71, 6, 123);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (72, 10, 200);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (73, 10, 31);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (74, 7, 59);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (75, 5, 299);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (76, 3, 153);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (77, 1, 53);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (78, 6, 180);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (79, 4, 273);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (80, 3, 273);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (81, 4, 60);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (82, 4, 56);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (83, 3, 23);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (84, 3, 29);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (85, 6, 277);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (86, 9, 35);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (87, 5, 279);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (88, 2, 283);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (89, 4, 91);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (90, 3, 148);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (91, 9, 40);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (92, 3, 121);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (93, 4, 68);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (94, 2, 54);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (95, 5, 196);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (96, 7, 269);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (97, 9, 3);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (98, 8, 166);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (99, 2, 155);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (100, 10, 214);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (101, 7, 85);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (102, 2, 273);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (103, 8, 174);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (104, 8, 124);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (105, 4, 189);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (106, 2, 130);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (107, 6, 32);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (108, 2, 173);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (109, 4, 166);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (110, 10, 11);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (111, 8, 173);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (112, 4, 206);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (113, 7, 282);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (114, 10, 230);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (115, 5, 256);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (116, 5, 230);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (117, 6, 280);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (118, 1, 199);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (119, 4, 91);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (120, 9, 265);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (121, 3, 203);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (122, 3, 35);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (123, 10, 224);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (124, 4, 61);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (125, 7, 237);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (126, 10, 56);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (127, 6, 228);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (128, 7, 222);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (129, 7, 30);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (130, 3, 106);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (131, 10, 44);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (132, 4, 60);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (133, 7, 236);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (134, 1, 27);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (135, 1, 190);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (136, 8, 163);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (137, 7, 81);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (138, 5, 216);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (139, 10, 31);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (140, 8, 24);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (141, 9, 61);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (142, 9, 74);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (143, 8, 240);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (144, 8, 173);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (145, 6, 65);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (146, 6, 95);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (147, 7, 113);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (148, 4, 105);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (149, 4, 249);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (150, 4, 106);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (151, 7, 292);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (152, 1, 138);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (153, 4, 170);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (154, 6, 260);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (155, 8, 272);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (156, 4, 82);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (157, 1, 194);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (158, 8, 220);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (159, 10, 105);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (160, 8, 228);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (161, 6, 186);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (162, 6, 40);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (163, 1, 100);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (164, 5, 100);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (165, 9, 16);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (166, 7, 199);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (167, 2, 120);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (168, 7, 215);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (169, 4, 190);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (170, 4, 300);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (171, 8, 84);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (172, 2, 201);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (173, 4, 257);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (174, 4, 44);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (175, 1, 250);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (176, 3, 56);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (177, 2, 100);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (178, 9, 256);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (179, 7, 246);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (180, 1, 51);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (181, 6, 10);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (182, 1, 161);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (183, 7, 71);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (184, 1, 92);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (185, 1, 217);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (186, 1, 2);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (187, 7, 80);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (188, 10, 60);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (189, 7, 36);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (190, 3, 119);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (191, 6, 263);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (192, 9, 109);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (193, 5, 204);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (194, 4, 296);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (195, 6, 206);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (196, 2, 222);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (197, 7, 162);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (198, 10, 175);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (199, 1, 148);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (200, 7, 211);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (201, 2, 183);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (202, 7, 261);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (203, 10, 106);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (204, 5, 245);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (205, 9, 154);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (206, 6, 185);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (207, 2, 288);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (208, 10, 32);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (209, 2, 203);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (210, 6, 72);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (211, 9, 193);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (212, 9, 125);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (213, 3, 38);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (214, 8, 154);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (215, 6, 52);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (216, 1, 65);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (217, 2, 102);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (218, 10, 240);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (219, 2, 154);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (220, 4, 235);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (221, 5, 274);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (222, 8, 201);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (223, 4, 94);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (224, 7, 247);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (225, 9, 149);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (226, 6, 192);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (227, 4, 24);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (228, 8, 157);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (229, 6, 168);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (230, 8, 135);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (231, 8, 109);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (232, 8, 88);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (233, 6, 42);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (234, 8, 235);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (235, 1, 121);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (236, 5, 229);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (237, 5, 177);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (238, 1, 136);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (239, 2, 122);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (240, 4, 16);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (241, 10, 142);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (242, 3, 229);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (243, 10, 262);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (244, 4, 264);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (245, 9, 183);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (246, 1, 124);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (247, 7, 222);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (248, 10, 55);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (249, 8, 241);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (250, 4, 107);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (251, 2, 254);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (252, 6, 180);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (253, 7, 213);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (254, 3, 119);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (255, 3, 267);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (256, 3, 166);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (257, 2, 41);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (258, 6, 119);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (259, 9, 60);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (260, 9, 260);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (261, 4, 63);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (262, 10, 37);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (263, 8, 106);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (264, 4, 27);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (265, 5, 148);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (266, 6, 122);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (267, 2, 185);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (268, 7, 92);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (269, 1, 74);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (270, 3, 216);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (271, 5, 257);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (272, 8, 298);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (273, 1, 74);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (274, 10, 155);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (275, 9, 277);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (276, 4, 62);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (277, 1, 1);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (278, 7, 114);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (279, 2, 222);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (280, 10, 248);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (281, 3, 164);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (282, 6, 19);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (283, 5, 261);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (284, 3, 217);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (285, 8, 69);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (286, 3, 267);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (287, 1, 227);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (288, 1, 14);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (289, 6, 60);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (290, 7, 64);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (291, 4, 223);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (292, 9, 144);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (293, 8, 265);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (294, 7, 30);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (295, 7, 283);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (296, 2, 259);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (297, 7, 41);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (298, 6, 28);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (299, 1, 21);
insert into POLOZKA (id_objednavka, pocet_kusu, id_specifikace)
values (300, 8, 154);

commit;

